<?php

return [
    /*
    |--------------------------------------------------------------------------
    | List your email providers
    |--------------------------------------------------------------------------
    |
    | Enjoy a life with multimail
    |
    */
    'use_default_mail_facade_in_tests' => true,
    'emails'  => [
        'naoresponder@iopoint.com.br' => [
            'pass'          => env('MAIL_PASSWORD_NAO_RESPONDER'),
            'from_name'     => 'Não Responda',
            'username'      => 'naoresponder@iopoint.com.br',
        ],
        'bemvindo@iopoint.com.br'  => [
            'pass'          => env('MAIL_PASSWORD_BEMVINDO'),
            'from_name'     => 'Bem Vindo',
            'username'      =>'bemvindo@iopoint.com.br',
        ],
        'recuperarsenha@iopoint.com.br'  => [
            'pass'          => env('MAIL_PASSWORD_RECUPERAR_SENHA'),
            'from_name'     => 'Recuperar Senha',
            'username'      =>'recuperarsenha@iopoint.com.br',
        ],
    ],

    'provider' => [
        'default' => [
            'host'      => env('MAIL_HOST'),
            'port'      => env('MAIL_PORT'),
            'encryption' => env('MAIL_ENCRYPTION'),
        ],
    ],

];
