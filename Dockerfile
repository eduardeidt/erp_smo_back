# GENERATE BUILDER IMAGE
FROM registry.gitlab.com/applicativa/infrastructure/docker-images:php-7.4-mysql-alpine-dev AS builder

ADD . /code

WORKDIR /code

# RUN composer install --prefer-dist --no-dev
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions http

RUN composer install --prefer-dist
RUN npm install && npm run prod
RUN rm -rf node_modules

# GENERATE PRODUCTION IMAGE
FROM registry.gitlab.com/applicativa/infrastructure/docker-images:php-7.4-mysql-alpine-base AS prod

COPY nginx-site.conf /etc/nginx/http.d/default.conf
COPY entrypoint.sh /etc/entrypoint.sh

RUN chmod +x /etc/entrypoint.sh

COPY --chown=www-data:www-data --from=builder /code /code

WORKDIR /code
EXPOSE 80

ENTRYPOINT ["/etc/entrypoint.sh"]