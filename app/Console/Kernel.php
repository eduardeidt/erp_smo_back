<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->call('\App\Http\Controllers\ProductPromotionController@importPromotionManually')->everyMinute();
//        $schedule->call('\App\Http\Controllers\PromotionController@schedulePromotion')->everyMinute();
//        $schedule->call('\App\Http\Controllers\Sync\SyncController@sync')->hourly();
        $schedule->call('\App\Http\Controllers\Sync\SyncController@sync')->everyFiveMinutes();
//        $schedule->call('\App\Http\Controllers\ProductController@calcTax')->hourly();
//        $schedule->call('\App\Http\Controllers\ProductPromotionController@importEduardPromotion')->everyMinute();
//        $schedule->call('\App\Http\Controllers\ConfigurationController@guiaDaFarmacia')->everyMinute();
//        $schedule->call('\App\Http\Controllers\ProductController@storeGuiaDaFarmacia')->daily();
//        $schedule->call('\App\Http\Controllers\ProductController@storeGuiaDaFarmacia')->everyMinute();

    }
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
