<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

trait Trackable
{
    public static function bootTrackable(){
        static::creating(function ($model) {
            Log::info(Auth::id());
            if (Schema::hasColumn($model->getTable(), 'created_by')) {
                $model->setAttribute('created_by', Auth::id());
            }
            if (Schema::hasColumn($model->getTable(), 'updated_by')) {
                $model->setAttribute('updated_by', Auth::id());
            }
        });

        static::updating(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'updated_by')) {
                $model->setAttribute('updated_by', Auth::id());
            }
        });

        static::deleting(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'deleted_by')) {
                $model->setAttribute('deleted_by', Auth::id());
            }
        });
    }
}
