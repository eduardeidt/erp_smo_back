<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MktFirstEmail extends Mailable{
    use Queueable, SerializesModels;
    public $subject;
    private $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data){
        $this->data = $data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $this->subject = "Boas vindas do sistema" ;
        return $this->view('Mail.Mkt.firstEmail')->with(['data'=>$this->data]);
    }
}
