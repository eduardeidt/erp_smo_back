<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Helpers\ReportsHelper;
use App\Models\Erp\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;


class ReportController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;

    public function __construct(Request $request){
        $this->request = $request;
    }
    public function productPriceLabel(){
        try {


            $arrayProducts = [];
            $productList = array_values(json_decode($this->request->productList));
            foreach ($productList as $productCode) {
                $productCodeMain = Product::getMainCodeByProduct($productCode) ?? $productCode;
                array_push($arrayProducts, $productCodeMain);
            }
            $pdf = PDF::loadView('Reports/productPriceLabel', ReportsHelper::generate_array_price_label($arrayProducts))
                ->setOption('page-width', '90')
                ->setOption('page-height', '30');
            return $pdf->download('Labels.pdf');
        } catch (\Exception $e){
            DB::rollBack();
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error','generic'),403);
        }

//        page-height

    }
}
