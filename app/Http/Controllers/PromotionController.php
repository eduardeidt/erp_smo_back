<?php


namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Helpers\SoftpharmaHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\Product;
use App\Models\Erp\ProductExtra;
use App\Models\Erp\ProductPromotion;
use App\Models\Erp\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PromotionController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        try {
            $promotionsActive = Promotion::where('status', 1)->get();
            $promotionsFinished = Promotion::where('status', 2)->get();
            $promotionsInAnalysis = Promotion::where('status', 3)->get();
            return response()->json(compact('promotionsActive', 'promotionsFinished', 'promotionsInAnalysis'), 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function store(){
        try {
            Log::error($this->request->end_date);
            $promotion = Promotion::create([
                'name' => $this->request->name,
                'begin_date' => $this->request->begin_date,
                'end_date' => $this->request->end_date,
                'status' => 3,
                'day_of_week' => implode(',', $this->request->day_of_week),
                'priority' => $this->request->priority,
            ]);
            return response()->json(array_merge(['id'=>$promotion->id],MessagesHelper::messages('success', 'create')), 201);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function show(int $id)
    {
        try {
            $promotion = Promotion::find($id);
            return response()->json($promotion, 200);
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function update(int $id) {
        try {
            $promotion = Promotion::findOrFail($id);
            $promotion->update([
                'name' => $this->request->name,
                'begin_date' => $this->request->begin_date,
                'end_date' => $this->request->end_date,
                'day_of_week' => implode(',', $this->request->day_of_week),
                'priority' => $this->request->priority,
            ]);
            $promotion->save();
            return response()->json($promotion, 200);
        } catch (\Exception $e) {
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function destroy(int $id) {
        try {
            DB::beginTransaction();
            $productPromotionFirst = ProductPromotion::where('promotion_id', $id)->first();
            if (is_null($productPromotionFirst)){
                Promotion::destroy($id);
            }else{
                $promotion = Promotion::find($id);
                $promotion->status=2;
                $promotion->end_date=now()->toDateString();
                $promotion->save();
                $productList = Product::where('promotion_id',$id)->get();
                if ($productList->isNotEmpty()){
                    foreach ($productList as $key =>$value){

                    }
                }
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'deleted'), 200);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function getNotInactive(){
        try {
            $arrayStatus =[
                '1'=>'A',
                '3'=>'E',
            ];
            $promotionsSql = Promotion::select('id','name','status')->whereIn('status',[1,3])->get();
            foreach ($promotionsSql as $key => $value){
                $promotions[] =[
                    'id'=>$value['id'],
                    'name'=>$value['name'],
                    'status'=>$arrayStatus[$value['status']]
                ];
            }
            return response()->json(compact('promotions'));
        } catch (\Exception $e) {
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function schedulePromotion(){
        try{
            $nowIso = Carbon::now()->isoWeekday();
            $yesterdayIso = Carbon::now()->yesterday()->isoWeekday();
            $now = now()->toDateString();
            /**
             * remove products from finished promotions
             */
            $promotionsEnd = Promotion::whereDate('end_date',$now)->get();
            foreach ($promotionsEnd as $d =>$promoData) {
                $this->removePromotion($promoData);
            }
            /**
             * add products from promotion
             */
            $promotions = Promotion::select('id','priority')
                ->whereDate('begin_date',$now)
//                ->orWhere(function($q) use ($now, $nowIso) {
//                    $q->whereDate('begin_date','<=',$now)
//                        ->where('day_of_week','like',"%{$nowIso}%");
//                })
                ->where('status',1)
                ->pluck('priority','id');
            foreach ($promotions as $promotionId =>$priority){
                $this->addProductPromotion($promotionId,$priority);
            }
            /**
             * get promotion when finish by week day
             *
             */

            $promotionList = Promotion::whereDate('begin_date','<=',$now)
                ->whereDate('end_date','>=',$now)
                ->where('status','1')
                ->pluck('id');

            foreach ($promotionList as $promotionId) {
                $promotion = Promotion::find($promotionId);
                if (!Helper::isAllDaysPromotion($promotion)){
                    Log::error($promotionId);
                    if (STR::contains($promotion->day_of_week,$yesterdayIso)
                        and !STR::contains($promotion->day_of_week,$nowIso)){
                        $this->removePromotion(['id'=>$promotion->id]);
                    }
                }
            }
//active promotions
            foreach ($promotionList as $promotionId) {
                $promotion = Promotion::find($promotionId);
                if (!Helper::isAllDaysPromotion($promotion)){
                    Log::error('21');
                    if (!STR::contains($promotion->day_of_week,$yesterdayIso)
                        and STR::contains($promotion->day_of_week,$nowIso)){
                        $this->addProductPromotion($promotion->id,$promotion->priority);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    private function removePromotion($promoData){
        $productPromotionEnd = ProductPromotion::where('promotion_id',$promoData['id'])->where('status',0)->get();
        foreach ($productPromotionEnd as $key => $product){
            $productPromotionId = Product::find($product['product_code'])->promotion_id;
            if ($productPromotionId == $promoData['id']){
                $branchs = Branch::getBranchs();
                foreach ($branchs as $branch){
                    $productExtraSingle = ProductExtra::find(['product_code' => $product['product_code'], 'branch_id' => $branch->id]);
                    $productExtraSingle->promotion_rule = '';
                    $productExtraSingle->save();
                }
                Product::where('code', $product['product_code'])
                    ->update(['old_promotion_id' => DB::raw('promotion_id')]);
                Product::where('code',$product['product_code'])->update(['promotion_id'=>NULL]);
                /**
                 * SoftPharma Update
                 **/
                $where = array(
                    'cad_codigo' => $product['product_code']
                );
                $update = array(
                    'cad_regra_promocao' => '',
                );
                SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
            }
        }
    }
    private function addProductPromotion($promotionId,$priority){
        $productsPromotion = ProductPromotion::where('promotion_id',$promotionId)->where('status',0)->get();
        foreach ($productsPromotion as $key => $product){
            $activePromotionId = Product::find($product['product_code'])->promotion_id ?? NULL;
            if (!is_null($activePromotionId)){
                $activePromotionPriority = Promotion::find($activePromotionId)->priority;
                if ($activePromotionPriority < $priority){
                    unset($productsPromotion[$promotionId]);
                    continue;
                }
            }
            $branchs = Branch::getBranchs();
            foreach ($branchs as $branch){
                $productExtraSingle = ProductExtra::find(['product_code' => $product['product_code'], 'branch_id' => $branch->id]);
                $productExtraSingle->promotion_rule = $product['promotion_rule'];
                $productExtraSingle->save();
            }
            Product::where('code', $product['product_code'])
                ->update(['old_promotion_id' => DB::raw('promotion_id')]);
            Product::where('code',$product['product_code'])->update(['promotion_id'=>$promotionId]);
            /*
            * SoftPharma Update
            */
            $profit = ProductExtra::where('product_code',$product['product_code'])->first()->profit;
            $where = array(
                'cad_codigo' => $product['product_code']
            );
            $update = array(
                'cad_regra_promocao' => $product['promotion_rule'],
//                        'cad_pcusto' => Helper::calc_cost($product['sell_value'],$profit)
            );
            SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
        }
    }

    public function getProductList($id): \Illuminate\Http\JsonResponse {
        try {
            $productList = Product::select(['product.code','product.bar_code','product.description','product.presentation',
                'product_promotion.sell_value','product_promotion.promotion_rule',
                'promotion.name as promotion_name','product.old_promotion_id'
            ])
                ->join('product_promotion','product_promotion.product_code','product.code')
                ->join('promotion','promotion.id','product_promotion.promotion_id')
                ->where('promotion.id', $id)
                ->get();
            if ($productList->isNotEmpty()){
                foreach ($productList as $product) {
                    $productData = Helper::calc_value_off($product['promotion_rule'],$product['sell_value'])[0];

                    $product['promotion_price'] = $productData['price'];
                    $product['promotion_perc'] = $productData['perc'];
//                    return response()->json(compact('product'));
                    if(!is_null($product['old_promotion_id'])){
                        $productLastPromotion = Promotion::select('promotion.name','product_promotion.sell_value','product_promotion.promotion_rule')
                            ->join('product_promotion','product_promotion.promotion_id','promotion.id')
                            ->where('promotion.id', $product['old_promotion_id'])
                            ->first();
                        $product['promotion_name_old'] = $productLastPromotion['name'];
                        $productDataOld = Helper::calc_value_off($productLastPromotion['promotion_rule'],$productLastPromotion['sell_value'])[0];
                        $product['promotion_price_old'] = $productDataOld['price'];
                        $product['promotion_perc_old'] = $productDataOld['perc'];
                    }else{
                        $product['promotion_price_old'] = '0.00';
                        $product['promotion_perc_old'] = '0.0';
                    }

                }
            }
            return response()->json(compact('productList'));

        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }

    }
}
