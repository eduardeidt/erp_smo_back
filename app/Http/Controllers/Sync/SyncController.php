<?php


namespace App\Http\Controllers\Sync;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\Lab;
use App\Models\Erp\Product;
use App\Models\Erp\ProductBar;
use App\Models\Erp\ProductBatch;
use App\Models\Erp\ProductExtra;
use App\Models\Erp\ProductSale;
use App\Models\Erp\ProductSubstance;
use App\Models\Erp\Substance;
use App\Models\Erp\TaxProduct;
use App\Models\Erp\TaxProductsPisCofin;
use App\Models\Erp\User;
use App\Models\SoftPharma\Carven;
use App\Models\SoftPharma\Estaux;
use App\Models\SoftPharma\Estbar;
use App\Models\SoftPharma\Estcad;
use App\Models\SoftPharma\Estcad1;
use App\Models\SoftPharma\Estlab;
use App\Models\SoftPharma\Estlig;
use App\Models\SoftPharma\Estlot;
use App\Models\SoftPharma\Ligaux;
use App\Models\SoftPharma\LotmovEntrada;
use App\Models\SoftPharma\Mtahistvda;
use App\Models\SoftPharma\Scfemp;
use App\Models\SoftPharma\Tribproduto;
use App\Models\SoftPharma\Tribprodutopiscofin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class SyncController extends Controller
{
    protected ?Request $request;
    const BRANCHS_TO_SYNC = ['1','3','4'];

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }
    public function sync(){
        try{
            $this->branches();
            $this->sellers();
            $this->labs();
            $this->products();
            $this->ProductBar();
            $this->productExtra();
            $this->productGroup();
            $this->tax();
            $this->taxPisCofins();
            $this->substancies();
            $this->productSubstancies();
            $this->sales();
            $this->batchs();

            if ($this->request->sync) {
                return response()->json(MessagesHelper::messages('success', 'synced'));
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }

    }

    private function branches()
    {
        try {
            $branches = Scfemp::select('scf_fg as id',
                'scf_razao as name',
                'scf_fantasia as description',
                'scf_cidade as city')
                ->whereIn('scf_fg', SELF::BRANCHS_TO_SYNC)
                ->get()
                ->toArray();
            Branch::insertOnDuplicateKey($branches);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function labs()
    {
        try {
            $labs = Estlab::select('lab_laboratorio as id', 'lab_razao as name', DB::raw('if (lab_situacao=0,1,0) as active'))
                ->get()
                ->toArray();
            Lab::insertOnDuplicateKey($labs);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function products()
    {
        /**
         * get Products from softpharma
         */
        $products = Estcad::select('cad_codigo as code',
            'cad_codigo',
            'cad_cod_barra as bar_code',
            'cad_tipo_produto as type_id',
            'tp.tdp_grupo_principal as group_id',
            'cad_laboratorio as lab_id',
            'cad_origem_prod as code_origin',
            'cad_descricao AS description',
            'cad_apresentacao AS presentation',
            'cad_qtde_caixa as fraction',
            DB::raw('if (cad_situacao=0,1,0) as active'))
            ->join('esttdp as tp', 'tp.tdp_tipo_produto', 'cad_tipo_produto')
            ->when($this->request->sync, function ($q) {
                return $q->limit(200)
                    ->orderByDesc('estcad.date_ins_upd');
            })
            ->get()
            ->toarray();
        DB::beginTransaction();
        $c = 0;
        foreach ($products as $key => $value) {
            $c++;
            if ($c == 5000) {
                $c = 0;
                DB::commit();
                DB::beginTransaction();
            }
            Product::updateOrCreate(['code' => $value['code']], $value);
        }
        DB::commit();
    }

    private function productExtra()
    {
        $productsExtra = Estcad1::select(
            'cad_pconsumo as sell_value',
            'cad_fg as branch_id',
            'cad_codigo as product_code',
            'cad_perc_lucro as profit',
            'cad_pcusto as cost_price',
            'cad_custo_medio as medium_cost',
            'cad_custo_medio_contabil as accounting_cost',
            'cad_regra_promocao as promotion_rule',
            'cad_qtde_estoque as stock',
            'cad_perc_comis_padrao as commission',
            'cad_dt_ult_cpr as last_purchase',
            'cad_dt_ult_vda as last_sale',
            'cad_curva_abc as abc_curve',
            'cad_dt_ult_alt as last_price_update')
            ->when($this->request->sync, function ($q) {
                return $q->limit(200)
                    ->orderByDesc('date_ins_upd');
            })
            ->whereIn('cad_fg', SELF::BRANCHS_TO_SYNC)
            ->get()
            ->toArray();
        DB::beginTransaction();
        $c = 0;
        foreach ($productsExtra as $key => $value) {
            $c++;
            if ($c == 10000) {
                $c = 0;
                DB::commit();
                DB::beginTransaction();
            }
            ProductExtra::updateOrCreate(['product_code' => $value['product_code'], 'branch_id' => $value['branch_id']], $value);
        }
        DB::commit();
    }
    private function sellers()
    {
        /**
         * 0 - Vendedor
         * 1 - Gerente
         * 2 - Supervisor
         * 3 - Farmaceutico
         */
        $sellers = Carven::select('ven_cod_vend as id', 'ven_nome as name', 'vinc_filial as branch_ids', 'pc.cfg_opcao as email')
            ->join('vinccarvenfg as v', 'v.vinc_colaborador', 'ven_cod_vend')
            ->join('parcfg as pc', function ($q) {
                $q->on('pc.cfg_sequencia', 'ven_cod_vend')
                    ->whereRaw('pc.cfg_programa = \'CAR118\'');
            })
            ->where('ven_situacao',0)
            ->get()
            ->toArray();
        foreach($sellers as $key =>$value){
            if ($value['branch_ids'] ==0){
                $d= Branch::select('id')->pluck('id')->toArray();
                $dd = implode(',',$d);
                $sellers[$key]['branch_ids'] = $dd;
            }
        }
        User::insertOnDuplicateKey($sellers);
        $users = User::all();
        $password = Hash::make('farmacia');
        foreach ($users as $user) {
            if ($user['password'] == '') {
                User::where('id', $user['id'])->update(array('password' => $password));
            }
        }
    }

    private function substancies(){
        $substancies = Estaux::select('aux_auxiliar AS code','aux_tipo AS type','aux_nome AS name')->get()->toArray();
        DB::beginTransaction();
        $c = 0;
        foreach ($substancies as $key => $value) {
            $c++;
            if ($c == 10000) {
                $c = 0;
                DB::commit();
                DB::beginTransaction();
            }
            Substance::updateOrCreate(['code' => $value['code'], 'type' => $value['type']], $value);
        }
        DB::commit();
    }
    private function productSubstancies(){
        $productSubstancies =  Ligaux::select('aux_codigo AS product_code','aux_tipo AS substance_type','aux_auxiliar AS substance_code')->get()->toArray();
        DB::beginTransaction();
        $c = 0;
        foreach ($productSubstancies as $key => $value) {
            $c++;
            if ($c == 10000) {
                $c = 0;
                DB::commit();
                DB::beginTransaction();
            }
            ProductSubstance::updateOrCreate(
                ['product_code' => $value['product_code'],
                    'substance_type' => $value['substance_type'],
                    'substance_code' => $value['substance_code']
                ],
                $value
            );
        }
        DB::commit();
    }
    private function sales()
    {
        $date = ProductSale::select('date')->max('date') ?? NULL;
        $sellData = Mtahistvda::select(
            'vda_data as date',
            'vda_fg as branch_id',
            'vda_colaborador as seller_id',
            'vda_produto as product_code',
            'vda_vlr_fat_bruto as total_value',
            'vda_vlr_desc as discount_value',
            'vda_vlr_fat_liq as net_value',
            'vda_vlr_cmv as value_cmv',
            'vda_qtde_itens as amount',
        )
            ->when($date, function ($q) use ($date) {
                $yesterday = (new Carbon($date))->subDay()->toDateString();
                ProductSale::whereDate('date', '>=', $yesterday)->delete();
                return $q->where('vda_data', '>=', $yesterday);
            })
            ->get()->toArray();

        foreach (array_chunk($sellData, 4000) as $t) {
            ProductSale::insert($t);
        }
    }

    private function taxPisCofins()
    {
        try {
            $tax = Tribprodutopiscofin::select(
                'trib_cod_produto as product_code',
                'trib_ini_vigencia as begin_date',
                'trib_tipo_tributacao as tax_type',
                'trib_cod_sped as code_sped',
                'trib_perc_pis_lucroreal as perc_pis_lucroreal',
                'trib_perc_cofins_lucroreal as perc_cofins_lucroreal',
                'trib_perc_pis_presumido as perc_pis_presumido',
                'trib_perc_cofins_presumido as perc_cofins_presumido',
                'trib_data_alteracao as change_date',
                'trib_usuario as user',
                'trib_data_proc_automatico as date_proc_automatico'
            )
                ->when($this->request->sync, function ($q) {
                    return $q->limit(200)
                        ->orderByDesc('trib_data_alteracao');
                })
                ->get()
                ->toArray();

            foreach (array_chunk($tax, 3000) as $t) {
                TaxProductsPisCofin::insertOnDuplicateKey($t);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function ProductBar()
    {
        $extraBarCode = Estbar::select('bar_codigo as product_code', 'bar_cod_barra as bar_code')
            ->join("estcad as e", function ($q) {
                $q->on('e.cad_codigo', 'bar_codigo')
                    ->where('e.cad_situacao', 0);
            })
            ->get()
            ->toArray();
        foreach ($extraBarCode as $key =>$value){
            ProductBar::updateOrCreate(['product_code'=>$value['product_code'],'bar_code'=>$value['bar_code']],[$value]);
        }
    }

//    private function taxRetained(){
//        try {
//            $tax = Tribprodutoretido::select(
//                'trib_cod_produto as product_code',
//                'trib_fg as branch_id',
//                'trib_bc_icms_ret as bc_icms_ret',
//                'trib_vlr_icms_ret as value_icms_ret',
//                'trib_vlr_icms_subst as value_icms_subst',
//                'trib_bc_fcp_ret as bc_fcp_ret',
//                'trib_vlr_fcp_ret as value_fcp_ret'
//            )
//                ->get()
//                ->toArray();
//
//            foreach (array_chunk($tax, 3000) as $t) {
//                TaxProductRetained::insertOnDuplicateKey($t);
//            }
//        } catch (\Exception $e) {
//            echo $e->getMessage();
//        }
//    }

    private function tax()
    {
        try {
            $tax = Tribproduto::select(
                'trib_cod_produto as product_code',
                'trib_tipo_tributacao as tipo_tributacao',
                'trib_aliq_icms as aliq_icms',
                'trib_uf_destino as uf',
                'trib_fundo_probreza as fundo_probreza',
                'trib_fg as branch',
                'trib_pmc as pmc',
                'trib_mva as mva',
                'trib_perc_red_bc_icms as perc_red_bc_icms',
                'trib_perc_red_bc_icms_st as perc_red_bc_icms_st',
                'trib_aliq_iss as aliq_iss',
                'trib_iss_cod_mun as iss_cod_mun',
                'trib_iss_atividade as iss_atividade',
                'trib_iss_natureza as iss_natureza',
                'trib_iss_cnae as iss_cnae',
                'trib_aliq_ecf as aliq_ecf',
                'trib_alteracao as alteracao',
                'trib_alteracao_usuario as alteracao_usuario',
                'trib_cod_beneficio as cod_beneficio',
                'trib_aliq_icms_desonerado as aliq_icms_desonerado',
                'trib_fundo_probreza_desonerado as fundo_probreza_desonerado')
                ->when($this->request->sync, function ($q) {
                    return $q->limit(200)
                        ->orderByDesc('trib_alteracao');
                })
                ->where('trib_uf_destino','sc')
                ->get()
                ->toArray();
            foreach (array_chunk($tax, 3000) as $t) {
                TaxProduct::insertOnDuplicateKey($t);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function productGroup()
    {
        $productsGroups = Estlig::select('lig_principal as product_code_main', 'lig_codigo as product_code_link')
            ->get()->toArray();
        $arrayCheck= $arrayProducts = [];
        foreach ($productsGroups as $key => $value) {
            $arrayProducts[] = $value['product_code_link'];
            $arrayProducts[] = $value['product_code_main'];
            Product::where('code', $value['product_code_link'])
                ->update(['product_code_main' => $value['product_code_main']]);
            if (!in_array($value['product_code_main'], $arrayCheck)) {
                Product::where('code', $value['product_code_main'])
                    ->update(['product_code_main' => $value['product_code_main']]);
                $arrayCheck[] = $value['product_code_main'];
            }
        }
        Product::whereNotIn('code',$arrayProducts)->update(['product_code_main' => NULL]);
    }

    private function batchs(){
        $estLot = LotmovEntrada::select(
            'mov_fg AS branch_id',
            'mov_codigo AS product_code',
            'mov_lote AS batch',
            'mov_vcto AS due_date',
            DB::raw('sum(mov_qtde_lote) AS amount'),
            'mov_reg_ms AS reg_ms'
        )
            ->whereIn('mov_fg', SELF::BRANCHS_TO_SYNC)
            ->groupBy('batch')
            ->groupBy('branch_id')
            ->get()
            ->toArray();
        DB::beginTransaction();
        $c =0;
        foreach ($estLot as $key => $value) {
            $c++;
            if ($c == 5000) {
                $c = 0;
                DB::commit();
                DB::beginTransaction();
            }
            ProductBatch::updateOrCreate(
                ['product_code'=>$value['product_code'],
                    'branch_id'=>$value['branch_id'],
                    'batch'=>$value['batch']
                ],
                $value
            );
        }
        DB::commit();
    }
}
