<?php

namespace App\Http\Controllers;

use App\Exports\GuiaDaFarmacia\Export;
use App\Helpers\MessagesHelper;
use App\Models\Erp\Configuration;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;


class ConfigurationController extends Controller
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function indexParameters()
    {
        $configuration = Configuration::first();
        return response()->json($configuration);
    }

    public function store()
    {
        try {
            Configuration::updateOrCreate(['id' => 1], [
                'icms1' => $this->request->icms1,
                'icms2' => $this->request->icms2,
                'icms3' => $this->request->icms3,
                'icms4' => $this->request->icms4,
                'irrf' => $this->request->irrf,
                'irrf_extra' => $this->request->irrfExtra,
                'social_contribution' => $this->request->socialContribution,
                'short_expiration_date' => $this->request->shortExpirationDate,
                'medium_expiration_date' => $this->request->mediumExpirationDate,
                'long_expiration_date' => $this->request->longExpirationDate,
            ]);
            return response()->json(MessagesHelper::messages('success', 'created'), 201);
        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function guiaDaFarmacia()
    {
        $export = new Export();
        $export->getDataGuiaDaFarmacia();
        $headers = ['Content-Type: application/txt'];
//        $path = storage_path('app/guiaDaFarmacia.txt');
//        return response()->download($path,NULL,$headers);
//            ->deleteFileAfterSend(true);
//        return response()->download($zip_file, null, $headers);
        echo "ok";

    }

}
