<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\Notification;
use App\Models\Erp\NotificationType;
use App\Models\Erp\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public function index()
    {
        $branchIds= json_decode($this->request->branch_ids);
        $notifications = Notification::select('notification.id', 'p.code','p.bar_code', 'p.description','p.presentation',
            'notification.observation', 'pe.sell_value', 'pe.promotion_rule','notification.branch_id')
            ->join('notification_type AS nt', 'nt.id', 'notification.notification_type_id')
            ->join('product AS p', 'p.code', 'notification.product_code')
            ->join('product_extra AS pe', function ($query) {
                return $query->on('pe.product_code', 'p.code')
                    ->whereColumn('notification.branch_id', 'pe.branch_id');
            })
            ->whereIn('notification.branch_id',$branchIds )
            ->where('notification.notification_type_id', $this->request->type)
            ->whereStatus(0)
            ->paginate(200);

        foreach ($notifications as $notification) {
            $notification->observation = nl2br($notification->observation);
            $notification->promo = Helper::calc_value_off($notification->promotion_rule, $notification->sell_value);
        }

        return response()->json(compact('notifications'));
    }

    public function update($id)
    {
        $notification = Notification::find($id);

        $notification->fill($this->request->toArray());

        $notification->save();

        return response()->json(compact('notification'));
    }
    public function updateList(){
        try {
            DB::beginTransaction();
            $arrayNotificationIds = json_decode($this->request->notificationListId);
            foreach ($arrayNotificationIds as $notificationId) {
                $notification = Notification::find($notificationId);
                if (!is_null($notification)) {

                    $groupedProducts = Product::getProductsByMainCode($notification->product_code);
                    if (empty($groupedProducts)) {
                        Notification::where('id', $notificationId)->update([
                            'status' => $this->request->status
                        ]);
                    }else{
                        foreach ($groupedProducts as $productCode) {
                            Notification::where([
                                'product_code' => $productCode,
                                'branch_id' => $notification->branch_id
                            ])->update([
                                'status' => $this->request->status
                            ]);
                        }
                    }
                }
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'update'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function delete($id){
        try{
            DB::beginTransaction();
            $notification = Notification::find($id);
            if (!is_null($notification)) {

                $groupedProducts = Product::getProductsByMainCode($notification->product_code);
                if (empty($groupedProducts)) {
                    Notification::destroy($id);
                }else{
                    foreach ($groupedProducts as $productCode){
                        $notification->where([
                            'product_code'=>$productCode,
                            'branch_id'=>$notification->branch_id
                        ])->delete();
                    }
                }
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success','delete'));
        }catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function deleteList(): \Illuminate\Http\JsonResponse{
        try {
            $arrayDeleteIds = json_decode($this->request->notificationListId);
            DB::beginTransaction();
            foreach ($arrayDeleteIds as $deleteId) {
                $notification = Notification::find($deleteId);
                if (!is_null($notification)) {
                    $groupedProducts = Product::getProductsByMainCode($notification->product_code);
                    if (empty($groupedProducts)) {
                        Notification::destroy($deleteId);
                    } else {
                        foreach ($groupedProducts as $productCode) {
                            $notification->where([
                                'product_code' => $productCode,
                                'branch_id' => $notification->branch_id
                            ])->delete();
                        }
                    }
                }
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'delete'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function store($product_code = null, $type = null, $observation = null)
    {
        if (!is_null($this->request)) {
            $type = $this->request->notification_type_id;
            $product_code = $this->request->product_code;
        }

        $product = Product::find($product_code);
        if (Helper::checkNotificationProduct($product)){
            $branchs = Branch::getBranchs();
            foreach ($branchs as $branch) {
                try {
                    Notification::updateOrCreate([
                        'product_code' => $product_code,
                        'notification_type_id' => $type,
                        'branch_id' => $branch->id,
                    ], [
                        'product_code' => $product_code,
                        'product_main_code' => $product->product_code_main,
                        'user_id' => 0,
                        'notification_type_id' => $type,
                        'status' => 0,
                        'branch_id' => $branch->id,
                        'title' => NotificationType::getTitle($type),
                        'date_limit' => '',
                        'appelant' => '0',
                        'observation' => $observation
                    ]);
                } catch (\Exception $e) {
                    Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
                    return response()->json(MessagesHelper::messages('error', 'generic'), 403);
//                return "{$e->getLine()} -> {$e->getMessage()}";
                }
            }
        }
    }
    /**
     * //    public function checkPriceNotifications(array $arrayProductsPriceSoft=>array $arrayProductsPromotionSoft){
     * //        $arrayProductsPriceErp = array();
     * //        $products = ProductExtra::whereProductCode(2)->get();
     * //        foreach ($products as $key =>$value){
     * //            $arrayProductsPriceErp[$value['product_code']][$value['branch_id']] = $value['sell_value'];
     * //            $arrayProductsPromotionErp[$value['product_code']][$value['branch_id']] = $value['promotion_rule'];
     * //        }
     * //        $result = Helper::array_diff_assoc_recursive($arrayProductsPromotionSoft, $arrayProductsPriceSoft);
     * ////        return response()->json($result);
     * ////        return response()->json($this->arrayProductsPriceSoft)->send();
     * //    }
     **/

}
