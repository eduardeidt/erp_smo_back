<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {

        $users = User::where('active',1)
        ->get()->map(function ($user) {
            $user->admin = $user->admin == 1 ? 'Sim' : 'Não';
            return $user;
        });
        return response()->json(compact('users'));
    }

    public function delete($id)
    {

        $user = User::find($id);
        $user->active = 0;
        $user->save();

        return response()->json(compact('user'));
    }


    public function update($id)
    {
        $user = User::find($id);
        $data = $this->request->toArray();
        if(!is_null($data['password'])) {
            $data['password'] = Hash::make($this->request->password);
        }
        else{
            unset($data['password']);
        }
        $user->fill($data);
        $user->save();

        return response()->json(compact('user'));
    }

    public function store()
    {
        try {
            $user = User::create([
                'user_type_id' => 0,
                'name' => $this->request->name,
                'name' => $this->request->name,
                'user' => $this->request->user,
                'email' => $this->request->email,
                'password' => Hash::make($this->request->password),
                'admin' => $this->request->admin,
                'active' => $this->request->active,
                'branch_ids' => $this->request->branch_ids,
            ]);
            return response()->json(MessagesHelper::messages('success', 'created'), 201);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function show($id)
    {
        $user = User::find($id);
        return response()->json(compact('user'));
    }
}

