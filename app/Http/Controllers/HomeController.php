<?php


namespace App\Http\Controllers;

use App\Helpers\MessagesHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\ProductSale;
use App\Models\Erp\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller {
    /**
     * @var Request
     */
    private Request $request;
    private int $code;

    public function __construct(Request $request) {
        $this->request = $request;
    }


    public function getHomeData() {
        try {
            $branchList = Branch::select('id','name')
                ->get();

            $userList = User::select('id','name')
                ->where('active',1)
                ->get();

            $homeData = [
                'branchList' => $branchList,
                'userList' => $userList,
                'monthData' => $this->getMonthData(true),
                'yearData' => $this->getYearData(true),
            ];

            return response()->json(compact('homeData'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function getMonthData(bool $returnAsFunction = false) {
        $branchId = $this->request->branchId != 0 ?$this->request->branchId : null;
        $userId = $this->request->userId != 0 ?$this->request->userId : null;
        $date = $this->request->date ?? Carbon::now()->format('Y-m-d');
        $monthYear = (new Carbon($date))->format('Ym');

        try {
            /* selectMothData */
            $monthDataSelected = ProductSale::select('date',DB::raw('sum(net_value) as net_value'))
                ->whereRaw("EXTRACT(YEAR_MONTH FROM date) = {$monthYear}")
//                ->whereYear('date',$date)
                ->when($branchId != null, function ($query) use ($branchId) {
                    $query->where('branch_id', $branchId);
                })
                ->when($userId != null, function ($query) use ($userId) {
                    $query->where('seller_id', $userId);
                })
                ->groupBy('date')
                ->get();

            $labelData = [];
            $valueData = [];
            foreach ($monthDataSelected as $day) {
                array_push($labelData,$day['date']);
                array_push($valueData,$day['net_value']);
            }

            $monthData = [
                'labelData' => $labelData,
                'valueData' => $valueData
            ];

            if ($returnAsFunction) {
                return $monthData;
            }

            return response()->json(compact('monthData'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function getYearData(bool $returnAsFunction = false) {
        $branchId = $this->request->branchId != 0 ?$this->request->branchId : null;
        $userId = $this->request->userId != 0 ?$this->request->userId : null;
        Log::warning($this->request->userId );
        Log::warning($userId );
        $monthYear = (new Carbon())->subMonths(12)->format('Ym');
        try {
            /* SelectProductSale */
            $monthDataSelected = ProductSale::select('date',DB::raw('sum(net_value) as net_value'))
                ->whereRaw("EXTRACT(YEAR_MONTH FROM date) >= {$monthYear}")
                ->when($branchId != null, function ($query) use ($branchId) {
                    $query->where('branch_id',$branchId);
                })
                ->when($userId != null, function ($query) use ($userId) {
                    $query->where('seller_id',$userId);
                })
                ->groupByRaw('EXTRACT(YEAR_MONTH FROM date)')
                ->get();

            $labelData = [];
            $valueData = [];
            foreach ($monthDataSelected as $day) {
                array_push($labelData,$day['date']);
                array_push($valueData,$day['net_value']);
            }

            $yearData = [
                'labelData' => $labelData,
                'valueData' => $valueData
            ];

            if ($returnAsFunction) {
                return $yearData;
            }

            return response()->json(compact('yearData'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

}

