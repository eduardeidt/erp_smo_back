<?php

namespace App\Http\Controllers;

use App\Helpers\MessagesHelper;
use App\Models\Erp\Configuration;
use App\Models\Erp\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{

    private $jwtAuth;

    public function __construct(JWTAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }

    public function login(Request $request, UserController $userController)
    {
        $user = User::select('id', 'name', 'email', 'user', 'password','admin','branch_ids')->where('user', $request->user)->first();
        $credentials = $request->only('user', 'password');
        $credentials['active']=1;
        if (!$token = $this->jwtAuth->attempt($credentials)) {
            return response()->json(MessagesHelper::messages('error', 'login_fail'), 401);
        }

        if (is_null($user) || !Hash::check($request->password, $user->password)) {
            return response()->json(MessagesHelper::messages('error', 'login_fail'), 401);
        }
        $configuration = Configuration::all();
        return response()->json(compact('token', 'user', 'configuration'));
    }


    public function refresh()
    {

        $token = $this->jwtAuth->getToken();
        $token = $this->jwtAuth->refresh($token);

        return response()->json(compact('token'));
    }

    public function logout()
    {
        $token = $this->jwtAuth->getToken();
        $this->jwtAuth->invalidate($token);

        return response()->json(['logout'], 200);
    }
}