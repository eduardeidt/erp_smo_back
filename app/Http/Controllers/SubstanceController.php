<?php

namespace App\Http\Controllers;

use App\Models\Erp\Substance;
use Illuminate\Http\Request;

class SubstanceController extends Controller{
    private Request $request;
    public function __construct(Request $request){
        $this->request = $request;
    }
    public function getSubstance() {
        $substance = Substance::select('id','name')
            ->where('type','0')
            ->where('name','like',"%{$this->request->name}%")
            ->get();
        return response()->json($substance);
    }
    public function getPharmacological() {
        $pharmacological = Substance::select('id','name')
            ->where('type','1')
            ->where('name','like',"%{$this->request->name}%")
            ->get();
        return response()->json($pharmacological);
    }
    public function getTherapeutic() {
        $terapeutic = Substance::select('id','name')
            ->where('type','2')
            ->where('name','like',"%{$this->request->name}%")
            ->groupBy('name')
            ->get();
        return response()->json($terapeutic);
    }
}
