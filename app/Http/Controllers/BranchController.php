<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Erp\Branch;
use App\Models\Erp\User;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $branchs = Branch::select('id', 'city')->get();
        $branchs->prepend(["id" => 0, "city" => "TODAS"]);

        return response()->json(compact('branchs'));
    }
}
