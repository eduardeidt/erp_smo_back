<?php


namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Helpers\SoftpharmaHelper;
use App\Models\Erp\Configuration;
use App\Models\Erp\Lab;
use App\Models\Erp\Product;
use App\Models\Erp\ProductBatch;
use App\Models\Erp\ProductExtra;
use App\Models\Erp\ProductProfit;
use App\Models\Erp\ProductPromotion;
use App\Models\Erp\Promotion;
use App\Models\SoftPharma\Esttdp;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductBatchController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /** @noinspection PhpUndefinedFieldInspection */
    public function index(): JsonResponse
    {
        try {
            $productListSql = Product::select('product.code', 'pb.id', 'pb.branch_id', 'b.city', 'pb.product_code', 'pb.batch',
                'pb.due_date', 'pb.amount', 'pb.reg_ms', 'pb.created_at', 'promo.name AS promotion_name',
                'product.description', 'product.presentation', 'product.bar_code', 'product.promotion_id', 'product.type_id', 'product.lab_id', 'product.group_id')
                ->with(array('product_extras.branch', 'product_extras' => function ($query) {
                    $query->select('product_code', 'sell_value', 'stock');
                }));
            if (!is_null($this->request->description)) {
                if (is_numeric($this->request->description)) {
                    $productListSql->leftjoin('product_bar AS pbar', 'pbar.product_code', 'product.code')
                        ->where(function ($q) {
                            $q->where('product.bar_code', $this->request->description)
                                ->orWhere('pbar.bar_code', $this->request->description)
                                ->orWhere('pb.batch', 'like', "%{$this->request->description}%");
                        });
                    $productListSql->limit(1);
                } else {
                    $productListSql->whereRaw("concat(product.description, ' ', product.presentation) like '%{$this->request->description}%'")
                    ->orWhere('pb.batch', 'like', "%{$this->request->description}%");
                }
            }
            
            if($this->request->branch_id){
                $productListSql->where('pb.branch_id', $this->request->branch_id);
            }

            if($this->request->typeList){
                $productListSql->whereIn('product.type_id', explode(',', $this->request->typeList));
            }

            if($this->request->labList){
                $productListSql->whereIn('product.lab_id', explode(',', $this->request->labList));
            }

            if($this->request->groupList){
                $productListSql->whereIn('product.group_id', explode(',', $this->request->groupList));
            }

            $productListSql->whereDate('pb.due_date', '>=', $this->request->beginDate)
                ->whereDate('pb.due_date', '<=', $this->request->endDate)
                ->where('pb.status', 1)
                ->join('product_batch AS pb', 'product.code', 'pb.product_code')
                ->join('branch as b', 'pb.branch_id', 'b.id')
                ->leftJoin('promotion AS promo', 'product.promotion_id', 'promo.id')
                ->orderBy('pb.due_date')
                ->orderBy('product.description');

            $productList = $productListSql->paginate(200);
            foreach ($productList as $product) {
                $dueDate = Carbon::parse($product['due_date']);
                $product['daysToExpire'] = Carbon::parse(now())->diffInDays($dueDate, false);
            }

            $labModel = new Lab();
            $dataLab = $labModel->getActiveLabs();
            $typesModel = new Esttdp();
            $dataType = $typesModel->getTypes();
            $dataGroup = SoftpharmaHelper::getGroups();

            $expirationDate = Configuration::select('short_expiration_date', 'medium_expiration_date', 'long_expiration_date')
                ->get();

            return response()->json(compact('dataLab', 'dataType', 'dataGroup', 'productList', 'expirationDate'));
        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function getDataForFilterProduct()
    {
        $labModel = new Lab();
        $dataLab = $labModel->getActiveLabs();
        $typesModel = new Esttdp();
        $dataType = $typesModel->getTypes();
        $dataGroup = SoftpharmaHelper::getGroups();
        $promotions = Promotion::get_active_promotions();
        return response()->json(compact('dataLab', 'dataType', 'dataGroup', 'promotions'));
    }

    public function getBatchListExpired()
    {
        try {
            $productList = Product::select('product.code', 'pb.status', 'pb.id', 'pb.branch_id', 'pb.product_code', 'pb.batch',
                'pb.due_date', 'pb.amount', 'pb.reg_ms', 'pb.created_at', 'promo.name AS promotion_name',
                'product.description', 'product.presentation', 'product.bar_code', 'product.promotion_id')
                ->where('pb.status', 1)
                ->whereDate('pb.due_date', '>=', $this->request->beginDate)
                ->whereDate('pb.due_date', '<=', $this->request->endDate)
                ->join('product_batch AS pb', 'product.code', 'pb.product_code')
                ->leftJoin('promotion AS promo', 'product.promotion_id', 'promo.id')
                ->orderBy('pb.due_date')
                ->orderBy('product.description')
                ->paginate(200)
                ->get();

            return response()->json(compact('productList'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function showExpiredProduct(int $request)
    {
        try {
            $product = ProductBatch::select('product_code',)
                ->when(!is_null($request), function ($p) use ($request) {
                    $p->where('product_batch.product_code', $request);
                })
                ->first();
            return response()->json($product);
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function inactiveProductBatch(int $id): JsonResponse
    {
        try {
            ProductBatch::where('id', $id)
                ->update(['status' => 0]);
            return response()->json($id, 200);
        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function updateExpirationDate(): JsonResponse
    {
        try {
            DB::beginTransaction();
            ProductExtra::where('branch_id', $this->request->branchId)
                ->where('product_code', $this->request->product_code)
                ->update(['manual_expiration_date' => $this->request->newExpirationDate]);
            ProductBatch::where('product_code', $this->request->product_code)
                ->whereDate('due_date', '<', $this->request->newExpirationDate)
                ->where('branch_id', $this->request->branchId)
                ->update(['status' => 0]);
            DB::commit();
            return response()->json($this->request->product_code, 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function store(): JsonResponse
    {
        try {
            $productCode = Product::getByBarCode($this->request->bar_code)->code ?? NULL;
            if (is_null($productCode)){
                return response()->json(MessagesHelper::messages('error', 'bar_code_invalid'), 403);
            }
            ProductBatch::create([
                'product_code'=> $productCode,
                'amount'=>$this->request->amount,
                'status'=>1,
                'batch'=> $this->request->batch,
                'due_date'=> $this->request->newExpirationDate,
                'branch_id'=> $this->request->branchId
            ]);
            return response()->json(MessagesHelper::messages('success', 'create'), 201);
        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
}

