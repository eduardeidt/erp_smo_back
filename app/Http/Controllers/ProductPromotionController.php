<?php


namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Helpers\PromotionHelper;
use App\Helpers\SoftpharmaHelper;
use App\Models\Erp\Branch;
use App\Models\Erp\Product;
use App\Models\Erp\ProductExtra;
use App\Models\Erp\ProductLink;
use App\Models\Erp\ProductPromotion;
use App\Models\Erp\ProductPromotionHistory;
use App\Models\Erp\Promotion;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductPromotionController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function store(){
        try {
            if ($this->request->new_promotion_id <> $this->request->old_promotion_id) {
                $this->update();
            }
            ProductPromotion::create();
            return response()->json(MessagesHelper::messages('success', 'created'), 201);
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function show(int $promotionId){
        try {
            $productsPromotion = ProductPromotion::wherePromotionId($promotionId)->get();
            return response()->json($productsPromotion, 200);
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function update(){
        try {
            $productPromotion = ProductPromotion::where('product_code', $this->request->product_code)
                ->where('promotion_id', $this->request->promotion_id)
                ->first();
            if ($this->request->promotion_rule){
                $this->request->promotion_rule = Helper::calc_percentage_off($this->request->promotion_rule);
            }
            $productPromotion->fill($this->request->all());
            $productPromotion->save();
            return response()->json(MessagesHelper::messages('success', 'updated'), 200);
        } catch (\Exception $e) {
//            return response()->json(($e->getMessage()));
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function destroy(){
        try {
            ProductPromotion::where('product_code', $this->request->product_code)
                ->where('promotion_id',$this->request->promotion_id)
                ->delete();
            return response()->json(MessagesHelper::messages('success', 'deleted'), 200);
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function deleteNextPromotion(int $id){
        try {
            Product::where('code',$id)->update(['old_promotion_id'=>NULL]);
            return response()->json(MessagesHelper::messages('success', 'deleted'), 200);
        } catch (\Exception $e) {
            Log::error($e->getFile().'-----'.$e->getLine().'------'.$e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }


    private function calcPromotionRuleJson(): string{
        $promos = json_decode($this->request->promos, true);
        $conditions = [];
        foreach ($promos as $condition) {
            $conditions[$condition['promo_quantity']] = str_replace(',', '.', $condition['promo_price']);
        }
        if (count($promos) > 0) {
            return  Helper::calc_percentage_off($conditions, $this->request->sell_value_original);
        } else {
            return  Helper::calc_percentage_off([1 => str_replace(',', '.', $this->request->new_price)], $this->request->sell_value_original);
        }

    }

    /**
     * @param $currentPromotion
     * @param $newPromotion
     * @return bool
     * compare 2 promotions priority! Return true if no change made
     *
     */
    private function checkPriority($currentPromotion, $newPromotion):bool{
        $newPromotionData = Promotion::find($newPromotion);
        $currentPromotionData = Promotion::find($currentPromotion);
        if (is_null($currentPromotionData)){
            return false;
        }
        if ($currentPromotionData->priority < $newPromotionData->priority) {
            return true;
        }
        return false;
    }

    public function promotion(){
        $promotion = Promotion::find($this->request->promotion);
        $product = Product::find($this->request->code);
        $nowIsoWeek = Carbon::now()->isoWeekday();
        $beginDate = now()->toDateString();
        if ($this->checkPriority($product->promotion_id,$this->request->promotion)) {
            return response()->json(MessagesHelper::messages('error','lowest_priority_promotion'),403);
        }
        $rules = $this->calcPromotionRuleJson();
        try {
            /**
             * check grouped and create products array
             */
            if ($this->request->grouped =='true') {
                $products = Product::getProductsByMainCode($this->request->code);
            } else {
                $products[] = $this->request->code;
            }
            /**
             *
             */
            foreach ($products as $productCode) {
                DB::beginTransaction();
                ProductPromotion::updateOrCreate([
                    'product_code' => $productCode,
                    'promotion_id' => $promotion->id
                ],
                    [
                        'promotion_id' => $promotion->id,
                        'product_code' => $productCode,
                        'sell_value' => $this->request->sell_value_original,
                        'status' => $promotion->status ==1 ? 0 : 1,
                        'promotion_rule' => $rules,
                    ]);
                if ($this->request->oldPromotion ==1){
                    Product::where('code', $productCode)
                        ->update(['old_promotion_id' => $this->request->promotion]);
                    DB::commit();
                    return response()->json(MessagesHelper::messages('success', 'updated'));

                }
                /**
                 * active product promotion on only active promotions
                 * and promotion day of week iso is configured
                 */

                if ($promotion->status == 1 and STR::contains($promotion->day_of_week,$nowIsoWeek)){
//                    /**
//                     * set status when has an active promotion
//                     */
//                    if ($product->promotion_id){
//                        ProductPromotion::where('product_code',$productCode)
//                            ->where('promotion_id',$product->promotion_id)
//                            ->update([
//                                'status'=>2,
//                            ]);
//                    }
                    PromotionHelper::storeProductPromotionHistory($productCode,$promotion->id,$this->request->sell_value_original,$rules,$beginDate,NULL);
                    $branchs = Branch::getBranchs();
                    Product::where('code', $productCode)
                        ->update(['old_promotion_id' => DB::raw('promotion_id')]);
                    Product::where('code',$productCode)->update(['promotion_id'=>$promotion->id]);
                    foreach ($branchs as $branch) {
                        $productExtraModel = ProductExtra::find(['product_code' => $productCode, 'branch_id' => $branch->id]);
                        $productExtraModel->update([
                            'promotion_rule' => $rules
                        ]);
                    }
                    Product::find($productCode)->touch();
                    DB::commit();
                    /**
                     * SoftPharma Update
                     */
                    $where = array(
                        'cad_codigo' => $productCode
                    );
                    $update = array(
                        'cad_regra_promocao' => $rules,
                    );
                    SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
                }else{
                    DB::commit();
                }
            }
            return response()->json(MessagesHelper::messages('success', 'updated'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function insertAnalyzeItem(){
        try{
            DB::beginTransaction();
            foreach ($this->request->code as $key =>$code){
                ProductPromotion::updateOrCreate(
                    [
                        'promotion_id'=>$this->request->promotion_id,
                        'product_code'=>$code,
                    ],
                    [
                        'promotion_id'=>$this->request->promotion_id,
                        'product_code'=>$code,
                        'status'=>1,
                    ]
                );
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success','create'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function confirmProductOnPromotion(){
        try{
            DB::beginTransaction();
            foreach ($this->request->productList as $key =>$product) {
                $products = [];
                if ($this->request->grouped =='true') {
                    $products = Product::getProductsByMainCode($product['code']);
                } else {
                    $products[] = $product['code'];
                }
                foreach ($products as $productCode) {
                    $arrayUpdateInsert = array();
                    if (isset($product['promotion_rule_new'])) {
                        $promotionProduct = $product['promotion_rule_new'];
                        foreach ($promotionProduct as $d => $promo) {
                            $arrayPromo[$promo['amount']] = $promo['price'];
                        }
                        $stringPromo = Helper::calc_percentage_off($arrayPromo, $product['sell_value']);
                        $arrayUpdateInsert['promotion_rule'] = $stringPromo;
                    }
                    $arrayUpdateInsert['status'] = $product['status'];
                    if (isset($product['sell_value'])) {
                        $arrayUpdateInsert['sell_value'] = $product['sell_value'];

                    }
                    $promotion = Promotion::findOrFail($this->request->promotion_id);
                    if ($product['status'] == 0){
                        $alter_sell_value = $product['alter_sell_value'] ? 'true': 'false';
                        $arrayUpdateInsert['alter_sell_value'] = $alter_sell_value === 'true' ? 1 : 0;
                    }
                    ProductPromotion::updateOrCreate([
                        'promotion_id' => $this->request->promotion_id,
                        'product_code' => $productCode,
                    ], $arrayUpdateInsert);
                }
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success','updated'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function getListToAnalyze(int $id){
        $fileMissingProducts =storage_path("app");
        $fileMissingProducts = file_exists("{$fileMissingProducts}/data/promotion/produtos_sem_cadastro_{$id}.csv");
        $productOnAnalysis = $productOnAnalyzed =[];
        $productsPromotion = ProductPromotion::select('p.bar_code','p.description','p.presentation','pe.promotion_rule','p.code','product_promotion.status',
            'pe.sell_value','l.name','product_promotion.promotion_rule as promo_promotion_rule','product_promotion.sell_value as promo_sell_value','code_origin')
            ->join('product as p','p.code','product_promotion.product_code')
            ->join('product_extra as pe','pe.product_code','p.code')
            ->join('lab as l','l.id','p.lab_id')
            ->where('product_promotion.promotion_id',$id)
            ->orderBy('p.description')
            ->groupBy('p.code')
            ->get();
        foreach ($productsPromotion as $key =>$productPromotion){
            if ($productPromotion['status'] == 1){
                $productPromotion['promotion_rule']= Helper::calc_value_off($productPromotion['promo_promotion_rule'],$productPromotion['sell_value']);
                $productOnAnalysis[] = $productPromotion;
            }else{
                $productPromotion['sell_value'] = $productPromotion['promo_sell_value'];
                $productPromotion['promotion_rule']= Helper::calc_value_off($productPromotion['promo_promotion_rule'],$productPromotion['sell_value']);
                $productOnAnalyzed[] = $productPromotion;
            }
        }

        return response()->json(compact('productOnAnalysis','productOnAnalyzed','fileMissingProducts'));
    }
    public function effectPromotion(){
        try{
            DB::beginTransaction();
            Promotion::where('id',$this->request->promotion_id)->update(['status'=>1]);
            if ((new Carbon(Promotion::find($this->request->promotion_id)->begin_date))->gt(new Carbon(now()->toDateString()))){
                DB::commit();
                return response()->json(MessagesHelper::messages('success', 'updated'), 200);
            }
            $productsPromotion = ProductPromotion::where('promotion_id',$this->request->promotion_id)->where('status',0)->get();
            $branchs = Branch::getBranchs();
            foreach ($productsPromotion as $key => $product){
                foreach ($branchs as $branch) {
                    $productExtraModel = ProductExtra::find(['product_code' => $product['product_code'], 'branch_id' => $branch->id]);
                    $productExtraModel->promotion_rule  = $product['promotion_rule'];
                    if ($product['alter_sell_value'] == 1){
                        $productExtraModel->sell_value = $product['sell_value'];
                        $profit = ProductExtra::where('product_code',$product['product_code'])->first()->profit;
                        $productExtraModel->cost_price =Helper::calc_cost($product['sell_value'],$profit);
                        $productExtraModel->sell_value = $product['sell_value'];
                    }
                    $productExtraModel->save();
                }
                $productSingle= Product::find($product['product_code']);
                $productSingle->update(['promotion_id'=>$this->request->promotion_id]);
                PromotionHelper::storeProductPromotionHistory($product['product_code'],$this->request->promotion_id,$product['sell_value'],
                    $product['promotion_rule'],$product['begin_date'],NULL);
            }
            DB::commit();
            /*
            * SoftPharma Update
            */
            foreach ($productsPromotion as $key => $product){
                $profit = ProductExtra::where('product_code',$product['product_code'])->first()->profit;
                $where = array(
                    'cad_codigo' => $product['product_code']
                );
                $update['cad_regra_promocao'] = $product['promotion_rule'];
                if ($product['alter_sell_value  ']==1) {
                    $update['cad_pcusto'] = Helper::calc_cost($product['sell_value'],$profit);
                    $update['cad_pconsumo'] = $product['sell_value'];
                }
                SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
            }
            return response()->json(MessagesHelper::messages('success', 'update'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function importCsvToAnalyze()
    {
        try {
            if ($this->request->hasFile('file')) {
                $path = $this->request->file('file')->getRealPath();
                $file = fopen($path, "r");
            } else {
                if($this->request->missing){
                    $file = fopen("../storage/app/data/promotion/produtos_sem_cadastro_{$this->request->promotion_id}.csv", "r");
                }else{
                    return response()->json(MessagesHelper::messages('error', 'invalid_file'), 403);
                }
            }
            Storage::disk("local")->delete("data/promotion/produtos_sem_cadastro_{$this->request->promotion_id}.csv");
            $filePath = "../storage/app/data/promotion/produtos_sem_cadastro_{$this->request->promotion_id}.csv";
            $fileReport=fopen($filePath,'w');
            DB::beginTransaction();
            while (($dataFile = fgetcsv($file, 0, ";")) !== FALSE) {
                $dataFile[0] = trim($dataFile[0]);
                $dataFile[3] = str_replace(',','.',trim($dataFile[3]));
                $productCode = Product::leftJoin('product_bar AS pb','product.code','pb.product_code')
                        ->where('product.bar_code',$dataFile[0])
                        ->orWhere('pb.bar_code',$dataFile[0])
                        ->first()->code ?? NULL;
                if (is_null($productCode)){
                    fwrite($fileReport,"{$dataFile[0]};{$dataFile[1]};{$dataFile[2]};{$dataFile[3]}\n");
                    continue;
                }
                $description = $dataFile[1];
                $sellValue = ProductExtra::firstWhere(['product_code'=>$productCode])->sell_value;
                if ($sellValue > $dataFile[3]){
                    $arrayPromo[1] = $dataFile[3];
                }else{
                    $arrayPromo[1] = $sellValue;
                }
                $promotion = Promotion::findOrFail($this->request->promotion_id);
                ProductPromotion::updateOrCreate(
                    [
                        'promotion_id'=>$this->request->promotion_id,
                        'product_code'=>$productCode,
                    ],
                    [
                        'promotion_id'=>$this->request->promotion_id,
                        'product_code'=>$productCode,
                        'sell_value' => $sellValue,
                        'promotion_rule' =>Helper::calc_percentage_off($arrayPromo,$sellValue),
                        'status'=>1,
                    ]
                );
            }
            fclose($fileReport);
            if(!filesize($filePath)) {
                unlink($filePath);
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'created'), 200);

        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }
    public function removeFromActivePromotion(){
        try {
            $now  = now()->toDateString();
            if ($this->request->grouped =='true') {
                $products = Product::getProductsByMainCode($this->request->product_code);
            } else {
                $products[] = $this->request->product_code;
            }
            foreach ($products as $productCode) {
                DB::beginTransaction();

                Product::where('code', $productCode)
                    ->update(['old_promotion_id' => DB::raw('promotion_id')]);
                Product::where('code', $productCode)
                    ->update(['promotion_id' => NULL]);
                $productExtras = ProductExtra::select('product_code', 'branch_id')
                    ->where('product_code', $productCode)
                    ->pluck('product_code', 'branch_id');
                foreach ($productExtras as $branch_id => $productCode) {
                    $productExtra = ProductExtra::firstWhere(['branch_id' => $branch_id, 'product_code' => $productCode]);
                    $productExtra->promotion_rule = '';
                    $productExtra->save();
                }
                PromotionHelper::updateProductPromotionHistory($productCode,$this->request->promotion_id,$now);
//                ProductPromotion::where([
//                    'promotion_id' => $this->request->promotion_id,
//                    'product_code' => $productCode
//                ])
//                    ->update(['end_date' => $now]);
                DB::commit();
                /**
                 * softpharma update
                 */
                $where = array(
                    'cad_codigo' => $productCode
                );
                $update = array(
                    'cad_regra_promocao' => '',
                );
                SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
            }
            return response()->json(MessagesHelper::messages('success', 'deleted'), 200);
        } catch (\Exception $e) {
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function showMissingProducts(){
        $filePath =storage_path("app");
        $filePath = "{$filePath}/data/promotion/produtos_sem_cadastro_{$this->request->promotion_id}.csv";
        return response()->download($filePath);
    }
    public function getProductsOutPromotion(){
        $promotionId = $this->request->promotion_id;
    }

    public function importEduardPromotion(){
        $file = fopen("promocao_produto.csv",'r');
        $fileWrite = fopen('promotions.csv','w');
        fwrite($fileWrite,"Codigo;Produto;Quantidade;Promocao;Preço_promo;Percentual;preço_original\n");
        while (($dataFile = fgetcsv($file, 0, ";")) !== FALSE) {
            $productCode = $dataFile['2'];
            $promotionRule = $dataFile['4'];
            $sellValue = ProductExtra::where('product_code',$productCode)->first()->sell_value;
            $calcSellValue = Helper::calc_value_off($promotionRule,$sellValue);
            $description = Product::selectRaw("CONCAT(description,'' ,presentation)  AS dd ")->where('code',$productCode)->first()->dd;
            foreach ($calcSellValue as $key =>$value){
                fwrite($fileWrite,"{$productCode};{$description};{$dataFile['10']};{$value['amount']};{$value['price']};{$value['perc']};{$sellValue}\n");
            }
        }
        fclose($fileWrite);
    }

    public function importPromotionManually(){
        $file = fopen("produto.csv",'r');
        while (($dataFile = fgetcsv($file, 0, ";")) !== FALSE) {
            $regraPromocao = $dataFile['3'];
            $promotionId = 1;
            echo "{$dataFile[0]}\n";
            $productCode = Product::getByBarCode($dataFile[0])->code;
            $product = Product::find($productCode);
            $product->promotion_id = $promotionId;
            $product->save();
            $productExtra = ProductExtra::where('product_code',$productCode)->get();
            if ($productExtra[0]['promotion_rule'] <>''){
                continue;
            }
            foreach ($productExtra as $key =>$value){
                $productE = ProductExtra::firstWhere('product_code',$productCode);
                $productE->promotion_rule = $regraPromocao;
                $productE->save();
            }
//            ProductPromotion::updateOrCreate([
//                'promotion_id'=>$promotionId,
//                'product_code'=>$productCode
//            ],[
//                'promotion_id'=>$promotionId,
//                'product_code'=>$productCode,
//                'sell_value'=>$productE->sell_value,
//                'promotion_rule'=>$regraPromocao,
//                'status'=>0
//
//            ]);
            $where = array(
                'cad_codigo' => $productCode
            );
            $update = array(
                'cad_regra_promocao' => $regraPromocao,
            );
            SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);

        }
    }
    public function getProductPromotionHistory(): JsonResponse{
        $productPromotionHistory = ProductPromotionHistory::select(['product_code','promotion_rule','sell_value','begin_date','end_date'])
            ->where('product_code',$this->request->product_code)
            ->get()->map(function ($productPromotionHistory) {
                $productPromotionHistory->promotion_rule = Helper::calc_value_off($productPromotionHistory->promotion_rule,$productPromotionHistory->sell_value);
                return $productPromotionHistory;
            });
        return response()->json($productPromotionHistory);
    }
}
