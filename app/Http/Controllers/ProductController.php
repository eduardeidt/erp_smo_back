<?php


namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Helpers\MessagesHelper;
use App\Helpers\SoftpharmaHelper;
use App\Models\Erp\Configuration;
use App\Models\Erp\GuiaDaFarmacia;
use App\Models\Erp\Lab;
use App\Models\Erp\Product;
use App\Models\Erp\ProductBatch;
use App\Models\Erp\ProductExtra;
use App\Models\Erp\ProductProfit;
use App\Models\Erp\Promotion;
use App\Models\SoftPharma\Esttdp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * @var Request
     */
    private Request $request;
    private int $code;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $products = Product::getProducts($this->request);
        foreach ($products as $key => $value) {
            $products[$key]['tax'] = $this->getCalcTaxData($value['code']);
            $products[$key]['promotion_rule'] = Helper::calc_value_off($value->product_extras[0]->promotion_rule, $value->product_extras[0]->sell_value);
            $products[$key]['product_extra_normal'] = $products[$key]['product_extras'][0];
            $products[$key]['product_extra_simple'] = $products[$key]['product_extras'][1];
            $products[$key]['product_extra_simple_two'] = $products[$key]['product_extras'][2];
            unset($products[$key]['product_extras']);
        }
        
        $productCodeList = array_column(array(...$products), 'code');
        $productExpireDate = ProductBatch::select('product_code','due_date')
            ->whereIn('product_code', $productCodeList)
            ->where('status',1)
            ->whereDate('due_date', '>=', now())
            ->groupBy('product_code') //TODO -
            ->orderBy('due_date') //TODO - confirm rule
            ->get();

        foreach ($products as $product) {
            $dueDateList = array_values(array_filter(array(...$productExpireDate), function ($productBatch) use ($product) {
                return $productBatch['product_code'] == $product['code'];
            }));
            $lastDueDate = count($dueDateList) > 0 ? $dueDateList[0]['due_date'] : null;
            if ($lastDueDate) {
                $daysToExpire = Carbon::parse(now())->diffInDays(Carbon::parse($lastDueDate), false);
            }else{
                $daysToExpire = 999;
            }
            $product['daysToExpire'] = $daysToExpire;
        }

        $expirationDate = Configuration::select('short_expiration_date', 'medium_expiration_date', 'long_expiration_date')->first();

        return response()->json(compact('products', 'expirationDate'));
    }
//    public function getSimple(){
//        $products = Product::getProducts($this->request,true);
//        foreach ($products as $key => $value) {
//            $products[$key]['tax'] = $this->getCalcTaxData($value['code'],true);
//            $products[$key]['promotion_rule'] = Helper::calc_value_off($value->product_extras[0]->promotion_rule, $value->product_extras[0]->sell_value);
//        }
//        return response()->json(compact('products'));
//    }

    public function getDataForFilterProduct()
    {
        $labModel = new Lab();
        $dataLab = $labModel->getActiveLabs();
        $typesModel = new Esttdp();
        $dataType = $typesModel->getTypes();
        $dataGroup = SoftpharmaHelper::getGroups();
        $promotions = Promotion::get_active_promotions();
        return response()->json(compact('dataLab', 'dataType', 'dataGroup', 'promotions'));
    }

    public function calcTax()
    {
        $product = Product::all();
        foreach ($product as $products => $value) {
            $productCode = $value['code'];
            $tax = $this->getCalcTaxData($productCode);
            $product = Product::find($productCode);
            $productExtra = ProductExtra::firstWhere('product_code', $productCode);
            if (isset($tax['icms'])) {
                if (!is_null($product->promotion_id)) {
                    $rulesPromotion = $productExtra->promotion_rule;
                    $arrayDataPromotion = Helper::calc_value_off($rulesPromotion, $productExtra->sell_value);
                    ProductProfit::where('product_code', $productCode)->delete();
                    foreach ($arrayDataPromotion as $key => $value) {
                        $amount = $value['amount'];
                        $sellValue = $value['price'];
                        $calculatedTaxs = Helper::cal_tax_product($tax['icms'], 12, $sellValue, $tax['contribSocial'], $tax['irrf'], $tax['extraIrrf'],
                            $tax['percPis'], $tax['percCofins'], $tax['mediumCost'], $tax['taxCode'], 0, 0, $tax['taxType']);
                        ProductProfit::create([
                            'product_code' => $productCode,
                            'amount' => $amount,
                            'profit_margin' => $calculatedTaxs['margemLiquida']
                        ]);
                    }
                } else {
                    $calculatedTaxs = Helper::cal_tax_product($tax['icms'], 12, $productExtra->sell_value, $tax['contribSocial'], $tax['irrf'], $tax['extraIrrf'],
                        $tax['percPis'], $tax['percCofins'], $tax['mediumCost'], $tax['taxCode'], 0, 0, $tax['taxType']);
                    ProductProfit::UpdateorCreate(
                        ['product_code' => $productCode,
                            'amount' => 1],
                        [
                            'product_code' => $productCode,
                            'amount' => 1,
                            'profit_margin' => $calculatedTaxs['margemLiquida']
                        ]);
                }
            }
        }
        return "ok";
        return response()->json($calculatedTaxs);

    }

    private function getCalcTaxData($product_code, $simple = false): array
    {
        $product = Product::with([
            'product_extras',
            'tax_products',
            'tax_products_pis_cofins'])
            ->where('code', $product_code)
            ->firstOrFail();

        if (!count($product->tax_products_pis_cofins) > 0) {
            return [];
        }

        if (!count($product->tax_products) > 0) {
            return [];
        }
        /*
         * TODO get parameters
         */
        $parameters = Configuration::first();
        $contribSocial = $parameters->social_contribution;
        $icms1 = $parameters->icms1;
        $icms2 = $parameters->icms2;
        $icms3 = $parameters->icms3;
        $icms4 = $parameters->icms4;
        $irrf = $parameters->irrf;
        $extraIrrf = $parameters->irrf_extra;
        $typeProduct = $product->code_origin;
        $icms = $product->tax_products[0]->aliq_icms;
        $taxType = $product->tax_products[0]->tipo_tributacao;
        $percPisPresumido = $product->tax_products_pis_cofins[0]->perc_pis_presumido;
        $percCofinsLucroPresumido = $product->tax_products_pis_cofins[0]->perc_cofins_presumido;
        $taxCode = $product->tax_products_pis_cofins[0]->tax_type;
        $mediumCostNormal = $product->product_extras[0]->medium_cost;
        $mediumCostSimple = $product->product_extras[1]->medium_cost;
        $sell_price = $product->product_extras[0]->sell_value;
        $accounting_cost = $product->product_extras[0]->accounting_cost;
        return array(
            'icms' => $icms,
            'sell_price' => $sell_price,
            'contribSocial' => $contribSocial,
            'irrf' => $irrf,
            'extraIrrf' => $extraIrrf,
            'percPis' => $percPisPresumido,
            'percCofins' => $percCofinsLucroPresumido,
            'mediumCostSimple' => $mediumCostSimple,
            'mediumCostNormal' => $mediumCostNormal,
            'accounting_cost' => $accounting_cost,
            'typeProduct' => $typeProduct,
            'taxCode' => $taxCode,
            'taxType' => $taxType,
            'icms1' => $icms1,
            'icms2' => $icms2,
            'icms3' => $icms3,
            'icms4' => $icms4,
        );
    }

    public function show($id)
    {
        try {
            $product = Product::with('product_extras')->findOrFail($id);
            return response()->json(compact('product'));
        } catch (\Exception $e) {
            return response()->json(MessagesHelper::messages('error', 'empty'));
        }
    }

    public function update()
    {
        try {
            $productCodeRequest = $this->request->code;
            if ($this->request->grouped == 'true') {
                $products = Product::getProductsByMainCode($this->request->code);
            } else {
                $products[] = $this->request->code;
            }
            $product = Product::firstWhere(['code' => $this->request->code]);
            $product->update([
                'description' => $this->request->description,
                'presentation' => $this->request->presentation,
            ]);
            foreach ($products as $productCode) {
                $productExtras = ProductExtra::select('product_code', 'branch_id')->where('product_code', $productCode)->pluck('product_code', 'branch_id');
                foreach ($productExtras as $branch_id => $productCode) {
                    $productExtra = ProductExtra::firstWhere(['branch_id' => $branch_id, 'product_code' => $productCode]);
                    $productExtra->sell_value = $this->request->sell_value;
                    $productExtra->cost_price = $this->request->cost_price;
                    $productExtra->profit = $this->request->profit;
                    $productExtra->last_price_update = now()->toDateString();
                    $productExtra->save();
                }
                Product::find($productCode)->touch();
                /**
                 * softpharma Update
                 */
                $where = array(
                    'cad_codigo' => $productCodeRequest
                );
                $update = array(
                    'cad_descricao' => $this->request->description,
                    'cad_apresentacao' => $this->request->presentation,
                );
                SoftpharmaHelper::updateSoftpharma('estcad', $update, $where);
                $where = array(
                    'cad_codigo' => $productCode
                );
                $update = array(
                    'cad_pconsumo' => $this->request->sell_value,
                    'cad_pcusto' => $this->request->cost_price,
                    'cad_perc_lucro' => $this->request->profit,
                    'cad_dt_ult_alt' => now()->toDateString()
                );
                SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
            }
            return response()->json(MessagesHelper::messages('success', 'updated'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function getByAutoComplete()
    {
        $this->request->description = trim($this->request->description);
        $productSql = Product::select('code', 'product.bar_code', 'product.product_code_main', 'description', 'presentation');
        if (!is_null($this->request->description)) {
            if (is_numeric($this->request->description)) {
                $productSql->leftjoin('product_bar as pb', 'pb.product_code', 'product.code')
                    ->where(function ($q) {
                        $q->where('product.bar_code', $this->request->description)
                            ->orWhere('pb.bar_code', $this->request->description);
                    });
            } else {
                $productSql->whereRaw("concat(description,' ',presentation) like '%{$this->request->description}%'");
            }
        }
        $product = $productSql->limit(40)->get();
        return response()->json($product);
    }


    public function inactive(int $code)
    {
        try {
            Product::where('code', $code)->update(['active' => 0]);
            /*
             * SoftPharma Update
             */
            $where = array(
                'cad_codigo' => $code
            );
            $update = array(
                'cad_situacao' => 1,
            );
            SoftpharmaHelper::updateSoftpharma('estcad', $update, $where);
            return response()->json(MessagesHelper::messages('success', 'deleted'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function storeGuiaDaFarmacia()
    {
        $url = "https://webservice.guiadafarmaciadigital.com.br/webservice/";
        for ($count = 1; $count <= 17; $count++) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $headers = array(
                "Content-Type: application/x-www-form-urlencoded",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $dataCurl = "cnpj_cpf=03678419000357&email=financeiro@dimeoeste.com.br&senha=226488&pagina={$count}";
            curl_setopt($curl, CURLOPT_POSTFIELDS, $dataCurl);
            $resp = curl_exec($curl);
            $data = json_decode($resp, true);
            curl_close($curl);
            DB::beginTransaction();
            foreach ($data['data'] as $key => $value) {
                if ($value['PRECO_MAXIMO_17'] != '0.00') {
                    $product = Product::getByBarCode($value['EAN']);
                    if (is_null($product)) {
                        continue;
                    }
                    $productCode = $product->code;
                    GuiaDaFarmacia::updateOrCreate(
                        ['product_code' => $productCode]
                        ,
                        [
                            'product_code' => $productCode,
                            'factory_price_17' => $value['PRECO_FABRICA_17'],
                            'max_price_17' => $value['PRECO_MAXIMO_17'],

                        ]
                    );
                }
            }
            DB::commit();
        }
    }

    public function indexGuiaDaFarmacia(): \Illuminate\Http\JsonResponse
    {
        $productListDB = Product::select(['code', 'bar_code', 'description', 'fraction', 'presentation', 'sell_value', 'cost_price', 'gdf.max_price_17', 'apply_fraction', 'fraction'])
            ->join('product_extra AS pe', 'pe.product_code', 'product.code')
            ->join('guia_da_farmacia AS gdf', 'gdf.product_code', 'product.code')
            ->whereColumn('gdf.max_price_17', '!=', 'pe.sell_value')
            ->where('active', 1)
            ->groupBy('code')
            ->paginate(250);

        $productList = [];
        foreach ($productListDB as $product) {
            $maxPrice17 = number_format($product['apply_fraction'] == '1' ? $product['max_price_17'] / $product['fraction'] : $product['max_price_17'], 2, '.', '');
            if ($maxPrice17 != $product['sell_value']) {
                array_push($productList, [
                    'code' => $product['code'],
                    'bar_code' => $product['bar_code'],
                    'description' => $product['description'],
                    'presentation' => $product['presentation'],
                    'sell_value' => $product['sell_value'],
                    'cost_price' => $product['cost_price'],
                    'apply_fraction' => $product['apply_fraction'],
                    'fraction' => $product['fraction'],
                    'max_price_17' => $maxPrice17,
                ]);
            }
        }

        $guideList = (object)[
            'data' => $productList,
            'last_page' => $productListDB->lastPage(),
            'current_page' => $productListDB->currentPage(),
        ];

        return response()->json(compact('guideList'));

    }

    public function updateGuiaDaFarmacia()
    {
        try {
            $productList = json_decode($this->request->productList);
            DB::beginTransaction();
            foreach ($productList as $productCode) {
                $guide = GuiaDaFarmacia::select('max_price_17', 'apply_fraction')->where('product_code', $productCode)->first();
                if ($guide->apply_fraction == 1) {
                    $fraction = Product::find($productCode)->fraction;
                    $sell_value = $guide->max_price_17 / $fraction;
                } else {
                    $sell_value = $guide->max_price_17;
                }
                $productExtras = ProductExtra::select('product_code', 'branch_id', 'profit')->where('product_code', $productCode)->get();
                foreach ($productExtras as $product) {
                    $productExtra = ProductExtra::firstWhere(['branch_id' => $product['branch_id'], 'product_code' => $productCode]);
                    $productExtra->sell_value = $sell_value;
                    $productExtra->cost_price = Helper::calc_cost($sell_value, $product['profit']);
                    $productExtra->save();
                }
                $where = array(
                    'cad_codigo' => $productCode
                );
                $update = array(
                    'cad_pconsumo' => $sell_value,
                    'cad_pcusto' => $productExtra->cost_price,
                );
                SoftpharmaHelper::updateSoftpharma('estcad1', $update, $where);
            }
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'update'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function setFractionGuiaDaFarmacia()
    {
        try {
            DB::beginTransaction();
            $productCode = $this->request->product_code;
            $applyFraction = $this->request->apply_fraction;
            GuiaDaFarmacia::where('product_code', $productCode)->update(['apply_fraction' => $applyFraction]);
            DB::commit();
            return response()->json(MessagesHelper::messages('success', 'update'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }

    public function deleteGuiaDaFarmacia()
    {
        try {
            $productCode = $this->request->product_code;
            GuiaDaFarmacia::where('product_code', $productCode)->delete();
            return response()->json(MessagesHelper::messages('success', 'delete'));
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getFile() . '-----' . $e->getLine() . '------' . $e->getMessage());
            return response()->json(MessagesHelper::messages('error', 'generic'), 403);
        }
    }


}

