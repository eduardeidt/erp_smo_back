<?php

namespace App\Providers;

use App\Models\Erp\ProductExtra;
use App\Observers\ProductExtraObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ProductExtra::observe(ProductExtraObserver::class);
    }
}
