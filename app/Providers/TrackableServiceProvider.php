<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class TrackableServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function register()
    {
        Blueprint::macro('trackables', function () {
            $this->integer('created_by')->unsigned()->nullable();
            $this->integer('updated_by')->unsigned()->nullable();
            $this->integer('deleted_by')->unsigned()->nullable();
        });

        Blueprint::macro('dropTrackables', function () {
            $this->dropColumn(['created_by', 'updated_by', 'deleted_by']);
        });
    }
}
