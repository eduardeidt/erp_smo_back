<?php

namespace App\Observers;

use App\Helpers\Helper;
use App\Http\Controllers\NotificationController;
use App\Models\Erp\ProductExtra;
use Illuminate\Support\Facades\Log;

class ProductExtraObserver
{
    /**
     * Handle the product extra "created" event.
     *
     * @param \App\Erp\ProductExtra $productExtra
     * @return void
     */
    public function created(ProductExtra $productExtra){
        $notification = new NotificationController();
        $notification->store($productExtra->product_code,2);
    }

    /**
     * Handle the product extra "updated" event.
     *
     * @param \App\Erp\ProductExtra $productExtra
     * @return void
     */
    public function updated(ProductExtra $productExtra)
    {
        if ($productExtra->sell_value <> $productExtra->getOriginal('sell_value')) {
            $notification = new NotificationController();
            $notification->store($productExtra->product_code, 2, "Valor produto  Alterado para {$productExtra->sell_value}");
        }

        if ($productExtra->promotion_rule <> $productExtra->getOriginal('promotion_rule')) {
            $promoCurrentString = "";
            $promoOriginalString = "";
            $notification = new NotificationController();
            $promosCurrent = Helper::calc_value_off($productExtra->promotion_rule, $productExtra->sell_value);
            foreach ($promosCurrent as $promoCurrent) {
                $promoCurrentString .= "{$promoCurrent['amount']}: R$ {$promoCurrent['price']} - {$promoCurrent['perc']} %\n";
            }
//            $promosOriginal = Helper::calc_value_off($productExtra->getOriginal('promotion_rule'), $productExtra->getOriginal('sell_value'));
//            foreach ($promosOriginal as $promoOriginal) {
//                $promoOriginalString .= "{$promoOriginal['amount']}: R$ {$promoOriginal['price']} - {$promoOriginal['perc']} %\n";
//            }
//            $notification->store($productExtra->product_code, 2, "Alteração de promoção:\nDe:\n{$promoOriginalString}\nPara:\n{$promoCurrentString}");
            $notification->store($productExtra->product_code, 2, "{$promoCurrentString}");
//            Log::error("Product Update (promotion_rule): {$productExtra->getOriginal('promotion_rule')} => {$productExtra->promotion_rule}");
        }
    }

    /**
     * Handle the product extra "deleted" event.
     *
     * @param \App\Erp\ProductExtra $productExtra
     * @return void
     */
    public function deleted(ProductExtra $productExtra)
    {

    }

    /**
     * Handle the product extra "restored" event.
     *
     * @param \App\Erp\ProductExtra $productExtra
     * @return void
     */
    public function restored(ProductExtra $productExtra)
    {
        //
    }

    /**
     * Handle the product extra "force deleted" event.
     *
     * @param \App\Erp\ProductExtra $productExtra
     * @return void
     */
    public function forceDeleted(ProductExtra $productExtra)
    {
        //
    }
}
