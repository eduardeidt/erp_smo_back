<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Replic extends  Model{
    protected $table = 'querygerada';
    protected $connection ='mysqlReplic';
    public $timestamps = false;
    protected $fillable = [
        'SQL' ,
        'idFilial',
        'DataMvto',
        'NivelUrgencia',
        'processada'
    ];

}
