<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tribprodutoretido
 * 
 * @property int $trib_fg
 * @property int $trib_cod_produto
 * @property float $trib_bc_icms_ret
 * @property float $trib_vlr_icms_ret
 * @property float $trib_vlr_icms_subst
 * @property float $trib_bc_fcp_ret
 * @property float $trib_vlr_fcp_ret
 *
 * @package App\Models
 */
class Tribprodutoretido extends Model
{
	protected $table = 'tribprodutoretido';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'trib_fg' => 'int',
		'trib_cod_produto' => 'int',
		'trib_bc_icms_ret' => 'float',
		'trib_vlr_icms_ret' => 'float',
		'trib_vlr_icms_subst' => 'float',
		'trib_bc_fcp_ret' => 'float',
		'trib_vlr_fcp_ret' => 'float'
	];

	protected $fillable = [
		'trib_bc_icms_ret',
		'trib_vlr_icms_ret',
		'trib_vlr_icms_subst',
		'trib_bc_fcp_ret',
		'trib_vlr_fcp_ret'
	];
}
