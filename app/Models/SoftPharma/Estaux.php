<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Estaux
 * 
 * @property float $aux_auxiliar
 * @property float $aux_tipo
 * @property string $aux_nome
 * @property string $aux_classif
 * @property string $aux_dcb
 * @property float $aux_gerar_info_sngpc
 * @property string $aux_registro_cas
 * @property int $aux_id_substancia_sngpc
 * @property int $aux_tipo_receita
 * @property int $aux_validade_receita
 * @property int $aux_lista
 * @property int $aux_adendo
 * @property int $aux_tipo_receita_adendo
 * @property int $aux_validade_receita_adendo
 *
 * @package App\Models
 */
class Estaux extends Model
{
	protected $table = 'estaux';
    protected $connection ='mysqlSoftPharma';
    public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'aux_auxiliar',
		'aux_tipo',
		'aux_nome',
		'aux_classif',
		'aux_dcb',
		'aux_gerar_info_sngpc',
		'aux_registro_cas',
		'aux_id_substancia_sngpc',
		'aux_tipo_receita',
		'aux_validade_receita',
		'aux_lista',
		'aux_adendo',
		'aux_tipo_receita_adendo',
		'aux_validade_receita_adendo'
	];
}
