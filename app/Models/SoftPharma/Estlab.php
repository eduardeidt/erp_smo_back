<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Estlab
 *
 * @property float $lab_laboratorio
 * @property string $lab_razao
 * @property string $lab_cnpj
 * @property float $lab_situacao
 * @property string $lab_contato
 * @property string $lab_fone
 * @property string $lab_email
 * @property string $lab_site
 * @property string $lab_observacao
 * @property string $lab_associadas
 * @property string $lab_nome_icone
 * @property int $lab_cpr_bloqueada
 * @property int $lab_forn_principal
 * @property Carbon $date_ins_upd
 *
 * @property Collection|Estcad[] $estcads
 *
 * @package App\Models
 */
class Estlab extends Model
{
	protected $table = 'estlab';
	protected $primaryKey = 'lab_laboratorio';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';
	protected $casts = [
		'lab_laboratorio' => 'float',
		'lab_situacao' => 'float',
		'lab_cpr_bloqueada' => 'int',
		'lab_forn_principal' => 'int'
	];

	protected $dates = [
		'date_ins_upd'
	];

	protected $fillable = [
		'lab_razao',
		'lab_cnpj',
		'lab_situacao',
		'lab_contato',
		'lab_fone',
		'lab_email',
		'lab_site',
		'lab_observacao',
		'lab_associadas',
		'lab_nome_icone',
		'lab_cpr_bloqueada',
		'lab_forn_principal',
		'date_ins_upd'
	];

	public function estcads()
	{
		return $this->hasMany(Estcad::class, 'cad_laboratorio');
	}

}
