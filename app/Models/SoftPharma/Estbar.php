<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Estbar
 * 
 * @property float $bar_codigo
 * @property string $bar_cod_barra
 *
 * @package App\Models
 */
class Estbar extends Model
{
	protected $table = 'estbar';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';


    protected $fillable = [
		'bar_codigo',
        'bar_cod_barra'
	];
}
