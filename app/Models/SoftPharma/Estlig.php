<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Estlig
 * 
 * @property float $lig_principal
 * @property float $lig_codigo
 * @property float $lig_app_regra_descto
 * 
 * @property Estcad $estcad
 *
 * @package App\Models
 */
class Estlig extends Model
{
	protected $table = 'estlig';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';



    protected $casts = [
		'lig_principal' => 'float',
		'lig_codigo' => 'float',
		'lig_app_regra_descto' => 'float'
	];

	protected $fillable = [
		'lig_app_regra_descto'
	];

	public function estcad()
	{
		return $this->belongsTo(Estcad::class, 'lig_codigo');
	}
}
