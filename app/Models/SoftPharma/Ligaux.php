<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ligaux
 *
 * @property float $aux_codigo
 * @property float $aux_tipo
 * @property float $aux_auxiliar
 * @property float $aux_is_principal
 * @property float $aux_cod_campanha
 *
 * @property Estcad $estcad
 *
 * @package App\Models
 */
class Ligaux extends Model
{
	protected $table = 'ligaux';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'aux_codigo' => 'float',
		'aux_tipo' => 'float',
		'aux_auxiliar' => 'float',
		'aux_is_principal' => 'float',
		'aux_cod_campanha' => 'float'
	];

	protected $fillable = [
		'aux_is_principal',
		'aux_cod_campanha'
	];

	public function estcad()
	{
		return $this->belongsTo(Estcad::class, 'aux_codigo');
	}
}
