<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Carven
 *
 * @property float $ven_cod_vend
 * @property string $ven_nome
 * @property float $ven_cidade
 * @property string $ven_ender
 * @property string $ven_bairro
 * @property string $ven_cep
 * @property string $ven_cpf
 * @property string $ven_rg
 * @property string $ven_telefone
 * @property Carbon $ven_dt_nascto
 * @property float $ven_tipo_comissao
 * @property string $ven_senha
 * @property string $ven_tipo
 * @property float $ven_situacao
 * @property float $ven_fg_pertence
 * @property float $ven_opcao_reducao_comissao
 * @property string $ven_user_fcia_popular
 * @property string $ven_pass_fcia_popular
 * @property float $ven_autorizado_cobrir_ofertas
 * @property string $ven_numero
 * @property string $ven_matricula
 * @property string $ven_conselho_numero
 * @property string $ven_uf_conselho
 * @property float $ven_expira_senha
 * @property Carbon $ven_dt_alteracao
 * @property Carbon $ven_dt_expiracao
 * @property float $ven_cod_bairro
 * @property float $ven_autorizado_liberar_alcada
 * @property float $ven_vlr_alcada
 * @property float $ven_autorizar_por
 * @property string $ven_operacoes_autorizado
 * @property int $ven_exigir_troca_senha
 * @property string $ven_celular
 * @property Carbon $ven_dt_admisao
 * @property string $ven_complemento
 * @property int $ven_fg_registro
 * @property string $ven_observacao
 * @property Carbon $date_ins_upd
 *
 * @property Collection|Carmet[] $carmets
 * @property Collection|Cartpv[] $cartpvs
 *
 * @package App\Models
 */
class Carven extends Model
{
	protected $table = 'carven';
    protected $connection ='mysqlSoftPharma';
    public $timestamps = false;

	protected $fillable = [
		'ven_nome',
		'ven_cidade',
		'ven_ender',
		'ven_bairro',
		'ven_cep',
		'ven_cpf',
		'ven_rg',
		'ven_telefone',
		'ven_dt_nascto',
		'ven_tipo_comissao',
		'ven_senha',
		'ven_tipo',
		'ven_situacao',
		'ven_fg_pertence',
		'ven_opcao_reducao_comissao',
		'ven_user_fcia_popular',
		'ven_pass_fcia_popular',
		'ven_autorizado_cobrir_ofertas',
		'ven_numero',
		'ven_matricula',
		'ven_conselho_numero',
		'ven_uf_conselho',
		'ven_expira_senha',
		'ven_dt_alteracao',
		'ven_dt_expiracao',
		'ven_cod_bairro',
		'ven_autorizado_liberar_alcada',
		'ven_vlr_alcada',
		'ven_autorizar_por',
		'ven_operacoes_autorizado',
		'ven_exigir_troca_senha',
		'ven_celular',
		'ven_dt_admisao',
		'ven_complemento',
		'ven_fg_registro',
		'ven_observacao',
		'date_ins_upd'
	];

	public function carmets()
	{
		return $this->hasMany(Carmet::class, 'MET_COD_VEND');
	}

	public function cartpvs()
	{
		return $this->hasMany(Cartpv::class, 'tpv_cod_vend');
	}
}
