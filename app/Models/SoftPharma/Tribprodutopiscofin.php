<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tribprodutopiscofin
 * 
 * @property int $trib_cod_produto
 * @property Carbon $trib_ini_vigencia
 * @property int $trib_tipo_tributacao
 * @property int $trib_cod_sped
 * @property float $trib_perc_pis_lucroreal
 * @property float $trib_perc_cofins_lucroreal
 * @property float $trib_perc_pis_presumido
 * @property float $trib_perc_cofins_presumido
 * @property Carbon $trib_data_alteracao
 * @property string $trib_usuario
 * @property Carbon $trib_data_proc_automatico
 *
 * @package App\Models
 */
class Tribprodutopiscofin extends Model
{
	protected $table = 'tribprodutopiscofins';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';


    protected $casts = [
		'trib_cod_produto' => 'int',
		'trib_tipo_tributacao' => 'int',
		'trib_cod_sped' => 'int',
		'trib_perc_pis_lucroreal' => 'float',
		'trib_perc_cofins_lucroreal' => 'float',
		'trib_perc_pis_presumido' => 'float',
		'trib_perc_cofins_presumido' => 'float'
	];

	protected $dates = [
		'trib_ini_vigencia',
		'trib_data_alteracao',
		'trib_data_proc_automatico'
	];

	protected $fillable = [
		'trib_tipo_tributacao',
		'trib_cod_sped',
		'trib_perc_pis_lucroreal',
		'trib_perc_cofins_lucroreal',
		'trib_perc_pis_presumido',
		'trib_perc_cofins_presumido',
		'trib_data_alteracao',
		'trib_usuario',
		'trib_data_proc_automatico'
	];
}
