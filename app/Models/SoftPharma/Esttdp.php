<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Esttdp
 *
 * @property float $tdp_tipo_produto
 * @property string $tdp_razao
 * @property float $tdp_margem_lucropos
 * @property float $tdp_margem_lucroneg
 * @property float $tdp_margem_lucroneu
 * @property float $tdp_liberar_descto_balcao
 * @property float $tdp_grupo_principal
 * @property float $tdp_emitir_etiquetas
 * @property float $tdp_altera_preco_venda
 * @property string $tdp_condicao_produto
 * @property float $tdp_sistema_fidelidade
 * @property float $tdp_perc_comissao
 * @property float $tdp_perc_comissaonv2
 * @property float $tdp_perc_comissaonv3
 * @property float $tdp_altera_descr
 * @property float $tdp_altera_preco_baixo
 * @property string $tdp_associadas
 * @property float $tdp_bloq_todos_descto
 * @property float $tdp_prom_cheque_pre
 * @property float $tdp_prom_tele
 * @property string $tdp_forn_upd_precos
 * @property float $tdp_situacao
 * @property float $tdp_perc_comissaonv4
 * @property float $tdp_exigir_ms
 * @property int $tdp_controlar_lotes
 * @property float $tdp_nao_upd_precos_forns_sem_pe
 * @property float $tdp_casas_decimais
 * @property float $tdp_exigir_receituario
 * @property float $tdp_tipo_receita
 * @property float $tdp_validade_receita
 * @property float $tdp_perc_comissao_promtabl
 * @property float $tdp_perc_comissaonv2_promtabl
 * @property float $tdp_perc_comissaonv3_promtabl
 * @property float $tdp_perc_comissaonv4_promtabl
 * @property int $tdp_id_tipo_sngpc
 * @property int $tdp_medicamento
 * @property int $tdp_perc_preco_venda
 * @property Carbon $date_ins_upd
 *
 * @property Collection|Cartpv[] $cartpvs
 * @property Collection|Estcad[] $estcads
 *
 * @package App\Models
 */
class Esttdp extends Model
{
	protected $table = 'esttdp';
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $fillable = [
	    'tdp_tipo_produto',
		'tdp_razao',
		'tdp_margem_lucropos',
		'tdp_margem_lucroneg',
		'tdp_margem_lucroneu',
		'tdp_liberar_descto_balcao',
		'tdp_grupo_principal',
		'tdp_emitir_etiquetas',
		'tdp_altera_preco_venda',
		'tdp_condicao_produto',
		'tdp_sistema_fidelidade',
		'tdp_perc_comissao',
		'tdp_perc_comissaonv2',
		'tdp_perc_comissaonv3',
		'tdp_altera_descr',
		'tdp_altera_preco_baixo',
		'tdp_associadas',
		'tdp_bloq_todos_descto',
		'tdp_prom_cheque_pre',
		'tdp_prom_tele',
		'tdp_forn_upd_precos',
		'tdp_situacao',
		'tdp_perc_comissaonv4',
		'tdp_exigir_ms',
		'tdp_controlar_lotes',
		'tdp_nao_upd_precos_forns_sem_pe',
		'tdp_casas_decimais',
		'tdp_exigir_receituario',
		'tdp_tipo_receita',
		'tdp_validade_receita',
		'tdp_perc_comissao_promtabl',
		'tdp_perc_comissaonv2_promtabl',
		'tdp_perc_comissaonv3_promtabl',
		'tdp_perc_comissaonv4_promtabl',
		'tdp_id_tipo_sngpc',
		'tdp_medicamento',
		'tdp_perc_preco_venda',
		'date_ins_upd'
	];

	public function cartpvs()
	{
		return $this->hasMany(Cartpv::class, 'tpv_tipo_produto');
	}

	public function estcads()
	{
		return $this->hasMany(Estcad::class, 'cad_tipo_produto');
	}
    public function getTypes() {
        return $this->select('tdp_tipo_produto as type_id','tdp_razao as description')->get();
    }
}
