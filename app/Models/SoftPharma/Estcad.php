<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Estcad
 *
 * @property float $cad_codigo
 * @property string $cad_cod_barra
 * @property string $cad_descricao
 * @property string $cad_apresentacao
 * @property float $cad_laboratorio
 * @property string $cad_descr_aux
 * @property float $cad_tipo_produto
 * @property string $cad_nome_lab
 * @property string $cad_nome_tp
 * @property string $cad_unimed
 * @property float $cad_perc_comis_padrao
 * @property float $cad_perc_comis_nivel2
 * @property float $cad_perc_comis_nivel3
 * @property float $cad_qtde_caixa
 * @property float $cad_continuo
 * @property float $cad_situacao
 * @property float $cad_qtde_embalagem
 * @property string $cad_ult_cod_barra
 * @property string $cad_mensagem
 * @property float $cad_lista
 * @property float $cad_utiliza_similar
 * @property float $cad_finalidade
 * @property string $cad_reg_ms
 * @property float $cad_pis_cofins
 * @property string $cad_ncm
 * @property float $cad_codigo_similar_principal
 * @property string $cad_mensagem_alerta
 * @property float $cad_vlr_referencia
 * @property float $cad_sazonalidade
 * @property Carbon $cad_dt_cadastro
 * @property string $cad_icone_similar_principal
 * @property int $cad_adendo
 * @property int $cad_falta_cronica
 * @property int $cad_representante_laboratorio
 * @property Carbon $cad_dt_ativacao
 * @property int $cad_origem_ativacao
 * @property int $cad_origem_prod
 * @property float $cad_prodpmc
 * @property string $cad_cest
 * @property Carbon $date_ins_upd
 *
 * @property Estlab $estlab
 * @property Esttdp $esttdp
 * @property Collection|Estbal[] $estbals
 * @property Collection|Estcad1[] $estcad1s
 * @property Collection|Estcnf[] $estcnfs
 * @property Collection|Estcnx[] $estcnxes
 * @property Collection|Estcpr[] $estcprs
 * @property Collection|Estcpr2[] $estcpr2s
 * @property Collection|Estde[] $estdes
 * @property Collection|Estfdl[] $estfdls
 * @property Collection|Estlct2t[] $estlct2ts
 * @property Collection|Estlct2v[] $estlct2vs
 * @property Collection|Estlig[] $estligs
 * @property Collection|Estlot[] $estlots
 * @property Collection|Estprc[] $estprcs
 * @property Collection|Estpsi[] $estpsis
 * @property Collection|Estrom2[] $estrom2s
 * @property Collection|Ligaux[] $ligauxes
 * @property Collection|Psisld[] $psislds
 *
 * @package App\Models
 */
class Estcad extends Model
{
    protected $table = 'estcad';
    public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

    protected $fillable = [
        'cad_codigo',
        'cad_cod_barra',
        'cad_descricao',
        'cad_apresentacao',
        'cad_laboratorio',
        'cad_descr_aux',
        'cad_tipo_produto',
        'cad_nome_lab',
        'cad_nome_tp',
        'cad_unimed',
        'cad_perc_comis_padrao',
        'cad_perc_comis_nivel2',
        'cad_perc_comis_nivel3',
        'cad_qtde_caixa',
        'cad_continuo',
        'cad_situacao',
        'cad_qtde_embalagem',
        'cad_ult_cod_barra',
        'cad_mensagem',
        'cad_lista',
        'cad_utiliza_similar',
        'cad_finalidade',
        'cad_reg_ms',
        'cad_pis_cofins',
        'cad_ncm',
        'cad_codigo_similar_principal',
        'cad_mensagem_alerta',
        'cad_vlr_referencia',
        'cad_sazonalidade',
        'cad_dt_cadastro',
        'cad_icone_similar_principal',
        'cad_adendo',
        'cad_falta_cronica',
        'cad_representante_laboratorio',
        'cad_dt_ativacao',
        'cad_origem_ativacao',
        'cad_origem_prod',
        'cad_prodpmc',
        'cad_cest',
        'date_ins_upd'
    ];

    public function estlab()
    {
        return $this->belongsTo(Estlab::class, 'cad_laboratorio');
    }

    public function esttdp()
    {
        return $this->belongsTo(Esttdp::class, 'cad_tipo_produto');
    }

    public function estbals()
    {
        return $this->hasMany(Estbal::class, 'bal_codigo');
    }

    public function estcad1s()
    {
        return $this->hasMany(Estcad1::class, 'cad_codigo','cad_codigo');
    }
    public function estcad1Sync()
    {
        return $this->hasMany(Estcad1::class, 'cad_codigo','cad_codigo')
            ->select(
                'cad_pconsumo as sell_value',
                'cad_fg as branch_id',
                'cad_codigo',
                'cad_codigo as product_code',
                'cad_perc_lucro as profit',
                'cad_pcusto as cost_price',
                'cad_custo_medio as medium_cost',
                'cad_regra_promocao as promotion_rule',
                'cad_qtde_estoque as stock',
                'cad_dt_ult_cpr as last_purchase',
                'cad_dt_ult_vda as last_sale',
                'cad_curva_abc as abc_curve');
    }

    public function estcnfs()
    {
        return $this->hasMany(Estcnf::class, 'CNF_CODIGO');
    }

    public function estcnxes()
    {
        return $this->hasMany(Estcnx::class, 'cnx_codigo');
    }

    public function estcprs()
    {
        return $this->hasMany(Estcpr::class, 'cpr_codigo');
    }

    public function estcpr2s()
    {
        return $this->hasMany(Estcpr2::class, 'cpr_codigo');
    }

    public function estdes()
    {
        return $this->hasMany(Estde::class, 'des_codigo');
    }

    public function estfdls()
    {
        return $this->hasMany(Estfdl::class, 'fdl_codigo');
    }

    public function estlct2ts()
    {
        return $this->hasMany(Estlct2t::class, 'lct_codigo');
    }

    public function estlct2vs()
    {
        return $this->hasMany(Estlct2v::class, 'lct_codigo');
    }

    public function estligs()
    {
        return $this->hasMany(Estlig::class, 'lig_codigo');
    }

    public function estlots()
    {
        return $this->hasMany(Estlot::class, 'LOT_CODIGO');
    }

    public function estprcs()
    {
        return $this->hasMany(Estprc::class, 'prc_codigo');
    }

    public function estpsis()
    {
        return $this->hasMany(Estpsi::class, 'psi_codigo');
    }

    public function estrom2s()
    {
        return $this->hasMany(Estrom2::class, 'rom_codigo');
    }

    public function ligauxes()
    {
        return $this->hasMany(Ligaux::class, 'aux_codigo');
    }

    public function psislds()
    {
        return $this->hasMany(Psisld::class, 'sld_codigo');
    }
}
