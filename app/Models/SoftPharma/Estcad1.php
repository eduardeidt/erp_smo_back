<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Estcad1
 *
 * @property float $cad_fg
 * @property float $cad_codigo
 * @property float $cad_pcusto
 * @property float $cad_pconsumo
 * @property float $cad_custo_medio
 * @property float $cad_perc_lucro
 * @property float $cad_est_minimo
 * @property float $cad_est_demanda
 * @property float $cad_qtde_estoque
 * @property string $cad_curva_abc
 * @property float $cad_tributacao
 * @property string $cad_data_vecto
 * @property string $cad_prateleira
 * @property Carbon $cad_dt_ult_alt
 * @property Carbon $cad_dt_ult_cpr
 * @property Carbon $cad_dt_ult_vda
 * @property Carbon $cad_dt_ult_bal
 * @property Carbon $cad_dt_ult_ped
 * @property float $cad_ult_pvenda
 * @property float $cad_perc_desc_basica
 * @property float $cad_perc_desc_vip1
 * @property float $cad_perc_desc_vip2
 * @property string $cad_ult_fornecedor
 * @property float $cad_ult_pcompra
 * @property float $cad_abc_giro_qtde
 * @property float $cad_abc_dias_dem
 * @property float $cad_pfabrica
 * @property string $cad_regra_promocao
 * @property string $cad_ult_update
 * @property float $cad_cpr_bloqueada
 * @property float $cad_demanda_fixada
 * @property float $cad_ult_pcompra1
 * @property float $cad_tabloide
 * @property float $cad_perc_comis_padrao
 * @property float $cad_perc_comis_nivel2
 * @property float $cad_perc_comis_nivel3
 * @property float $cad_perc_comis_nivel4
 * @property float $cad_id_pis
 * @property float $cad_id_cofins
 * @property string $cad_regra_promocao_descto_minimo
 * @property float $cad_autorizado_cobrir_ofertas
 * @property float $cad_mix
 * @property float $cad_perc_comis_padrao_promtabl
 * @property float $cad_perc_comis_nivel2_promtabl
 * @property float $cad_perc_comis_nivel3_promtabl
 * @property float $cad_perc_comis_nivel4_promtabl
 * @property float $cad_custo_contabil
 * @property float $cad_custo_medio_contabil
 * @property string $cad_cod_trib_mun
 * @property int $cad_qtde_ped_base
 * @property Carbon $date_ins_upd
 *
 * @property Estcad $estcad
 * @property Scfemp $scfemp
 *
 * @package App\Models
 */
class Estcad1 extends Model
{
	protected $table = 'estcad1';
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';
	protected $fillable = [
	    'cad_codigo',
		'cad_pcusto',
		'cad_pconsumo',
		'cad_custo_medio',
		'cad_perc_lucro',
		'cad_est_minimo',
		'cad_est_demanda',
		'cad_qtde_estoque',
		'cad_curva_abc',
		'cad_tributacao',
		'cad_data_vecto',
		'cad_prateleira',
		'cad_dt_ult_alt',
		'cad_dt_ult_cpr',
		'cad_dt_ult_vda',
		'cad_dt_ult_bal',
		'cad_dt_ult_ped',
		'cad_ult_pvenda',
		'cad_perc_desc_basica',
		'cad_perc_desc_vip1',
		'cad_perc_desc_vip2',
		'cad_ult_fornecedor',
		'cad_ult_pcompra',
		'cad_abc_giro_qtde',
		'cad_abc_dias_dem',
		'cad_pfabrica',
		'cad_regra_promocao',
		'cad_ult_update',
		'cad_cpr_bloqueada',
		'cad_demanda_fixada',
		'cad_ult_pcompra1',
		'cad_tabloide',
		'cad_perc_comis_padrao',
		'cad_perc_comis_nivel2',
		'cad_perc_comis_nivel3',
		'cad_perc_comis_nivel4',
		'cad_id_pis',
		'cad_id_cofins',
		'cad_regra_promocao_descto_minimo',
		'cad_autorizado_cobrir_ofertas',
		'cad_mix',
		'cad_perc_comis_padrao_promtabl',
		'cad_perc_comis_nivel2_promtabl',
		'cad_perc_comis_nivel3_promtabl',
		'cad_perc_comis_nivel4_promtabl',
		'cad_custo_contabil',
		'cad_custo_medio_contabil',
		'cad_cod_trib_mun',
		'cad_qtde_ped_base',
		'date_ins_upd'
	];

	public function estcad()
	{
		return $this->belongsTo(Estcad::class, 'cad_codigo');
	}

	public function scfemp()
	{
		return $this->belongsTo(Scfemp::class, 'cad_fg');
	}
}
