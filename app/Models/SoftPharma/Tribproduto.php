<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tribproduto
 * 
 * @property int $trib_cod_produto
 * @property string $trib_uf_destino
 * @property int $trib_tipo_tributacao
 * @property float $trib_aliq_icms
 * @property float $trib_fundo_probreza
 * @property float $trib_pmc
 * @property float $trib_mva
 * @property int $trib_fg
 * @property float $trib_perc_red_bc_icms
 * @property float $trib_perc_red_bc_icms_st
 * @property float $trib_aliq_iss
 * @property string $trib_iss_cod_mun
 * @property string $trib_iss_atividade
 * @property int $trib_iss_natureza
 * @property string $trib_iss_cnae
 * @property float $trib_aliq_ecf
 * @property Carbon $trib_alteracao
 * @property string $trib_alteracao_usuario
 * @property string $trib_cod_beneficio
 * @property float $trib_aliq_icms_desonerado
 * @property float $trib_fundo_probreza_desonerado
 *
 * @package App\Models
 */
class Tribproduto extends Model
{
	protected $table = 'tribproduto';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'trib_cod_produto' => 'int',
		'trib_tipo_tributacao' => 'int',
		'trib_aliq_icms' => 'float',
		'trib_fundo_probreza' => 'float',
		'trib_pmc' => 'float',
		'trib_mva' => 'float',
		'trib_fg' => 'int',
		'trib_perc_red_bc_icms' => 'float',
		'trib_perc_red_bc_icms_st' => 'float',
		'trib_aliq_iss' => 'float',
		'trib_iss_natureza' => 'int',
		'trib_aliq_ecf' => 'float',
		'trib_aliq_icms_desonerado' => 'float',
		'trib_fundo_probreza_desonerado' => 'float'
	];

	protected $dates = [
		'trib_alteracao'
	];

	protected $fillable = [
        'trib_cod_produto',
		'trib_tipo_tributacao',
		'trib_aliq_icms',
		'trib_fundo_probreza',
		'trib_pmc',
		'trib_mva',
		'trib_perc_red_bc_icms',
		'trib_perc_red_bc_icms_st',
		'trib_aliq_iss',
		'trib_iss_cod_mun',
		'trib_iss_atividade',
		'trib_iss_natureza',
		'trib_iss_cnae',
		'trib_aliq_ecf',
		'trib_alteracao',
		'trib_alteracao_usuario',
		'trib_cod_beneficio',
		'trib_aliq_icms_desonerado',
		'trib_fundo_probreza_desonerado'
	];
}
