<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Mtahistvda
 *
 * @property Carbon $vda_data
 * @property int $vda_fg
 * @property int $vda_colaborador
 * @property int $vda_is_gerente
 * @property int $vda_produto
 * @property float $vda_vlr_fat_bruto
 * @property float $vda_vlr_desc
 * @property float $vda_vlr_fat_liq
 * @property float $vda_vlr_cmv
 * @property int $vda_qtde_itens
 *
 * @package App\Models
 */
class Mtahistvda extends Model
{
	protected $table = 'mtahistvda';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'vda_fg' => 'int',
		'vda_colaborador' => 'int',
		'vda_is_gerente' => 'int',
		'vda_produto' => 'int',
		'vda_vlr_fat_bruto' => 'float',
		'vda_vlr_desc' => 'float',
		'vda_vlr_fat_liq' => 'float',
		'vda_vlr_cmv' => 'float',
		'vda_qtde_itens' => 'int'
	];

	protected $dates = [
		'vda_data'
	];

	protected $fillable = [
		'vda_vlr_fat_bruto',
		'vda_vlr_desc',
		'vda_vlr_fat_liq',
		'vda_vlr_cmv',
		'vda_qtde_itens'
	];
}
