<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Parcfg
 *
 * @property string $cfg_programa
 * @property float $cfg_sequencia
 * @property string $cfg_opcao
 *
 * @package App\Models
 */
class Parcfg extends Model
{
	protected $table = 'parcfg';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'cfg_sequencia' => 'float'
	];
}
