<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Estlot
 * 
 * @property float $LOT_FG
 * @property float $LOT_CODIGO
 * @property string $LOT_LOTE
 * @property Carbon $LOT_VCTO
 * @property float $lot_qtde
 * @property string $lot_reg_ms
 * 
 * @property Estcad $estcad
 *
 * @package App\Models
 */
class Estlot extends Model
{
	protected $table = 'estlot';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $fillable = [
	    'LOT_FG',
		'LOT_CODIGO',
        'LOT_LOTE',
	    'LOT_VCTO',
		'lot_qtde',
		'lot_reg_ms'
	];
	public function estcad(){
		return $this->belongsTo(Estcad::class, 'LOT_CODIGO');
	}
}
