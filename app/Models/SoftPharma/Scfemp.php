<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Scfemp
 *
 * @property float $scf_fg
 * @property string $scf_razao
 * @property string $scf_fantasia
 * @property string $scf_endereco
 * @property string $scf_cidade
 * @property string $scf_estado
 * @property string $scf_bairro
 * @property string $scf_cep
 * @property string $scf_telefone
 * @property string $scf_fax
 * @property string $scf_cnpj
 * @property string $scf_inscricao
 * @property Carbon $scf_data_proc
 * @property float $scf_juro_mora_mes
 * @property string $scf_senha_liber
 * @property string $scf_senha_liber_cli
 * @property string $scf_senha_liber_rec
 * @property string $scf_senha_liber_estneg
 * @property string $scf_senha_liber_prevda
 * @property string $scf_senha_liber_precpr
 * @property string $scf_senha_liber_trnvda
 * @property Carbon $scf_ult_backup
 * @property string $scf_dt_bl
 * @property float $scf_tipo
 * @property float $scf_cliente
 * @property string $scf_usuario
 * @property string $scf_versao
 * @property float $scf_log_write
 * @property float $scf_log_read
 * @property float $scf_mes_atual
 * @property string $scf_prop_email
 * @property Carbon $scf_prop_dt_nasc
 * @property string $scf_prop_nome
 * @property Carbon $scf_prop_dt_ult_envio
 * @property Carbon $scf_ult_dt_update
 * @property string $scf_versao_indicador
 * @property string $scf_complemento
 * @property float $scf_perc_pis
 * @property float $scf_perc_cofins
 * @property string $scf_senha_liber_rec_jur
 * @property string $scf_endereco_num
 * @property string $scf_inscr_mun
 * @property string $scf_versao_build
 * @property float $scf_enquadramento
 * @property float $scf_perfil_tributario
 * @property float $scf_cod_cidade
 * @property string $scf_email
 * @property float $scf_perc_multa_atraso
 * @property float $scf_sintegra_valores_nf_x929
 * @property string $scf_sintegra_registro_60
 * @property string $scf_senha_liber_desctos
 * @property float $scf_tamanho_loja
 * @property float $scf_qtde_colaboradores
 * @property float $scf_filial_gerenciada_plugpharma
 * @property int $scf_assoc_franquia_plugpharma
 * @property int $scf_versao_id
 * @property int $scf_inscrita_programa_credito
 * @property float $scf_aliq_icms_simples
 * @property int $scf_sintegra_valor_60i
 * @property string $scf_dt_vcto_boleto
 * @property int $scf_dt_show_aviso_boleto
 * @property float $scf_icms_vda_ecf_nfce_sat
 * @property float $scf_csosn_nfce_sat
 * @property float $scf_cson_contribuinte_icms
 * @property float $scf_cson_nao_contribuinte_icms
 * @property float $scf_par_faixa_faturamento
 * @property string $scf_identificacao
 * @property float $scf_csosn_nao_contribuinte_prodvda
 * @property float $scf_csosn_contribuinte_prodvda
 * @property string $scf_cnae
 * @property Carbon $date_ins_upd
 *
 * @property Collection|Carmot[] $carmots
 * @property Collection|Cpaneg[] $cpanegs
 * @property Collection|Estcad1[] $estcad1s
 * @property Collection|Estde[] $estdes
 * @property Collection|Estrom1[] $estrom1s
 * @property Collection|Indimo[] $indimos
 * @property Collection|Indvlr[] $indvlrs
 * @property Collection|Ofepar[] $ofepars
 * @property Collection|Parcmp[] $parcmps
 * @property Scfmsg $scfmsg
 * @property Scfpar $scfpar
 * @property Scfram $scfram
 *
 * @package App\Models
 */
class Scfemp extends Model
{
	protected $table = 'scfemp';
	protected $primaryKey = 'scf_fg';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';

	protected $casts = [
		'scf_fg' => 'float',
		'scf_juro_mora_mes' => 'float',
		'scf_tipo' => 'float',
		'scf_cliente' => 'float',
		'scf_log_write' => 'float',
		'scf_log_read' => 'float',
		'scf_mes_atual' => 'float',
		'scf_perc_pis' => 'float',
		'scf_perc_cofins' => 'float',
		'scf_enquadramento' => 'float',
		'scf_perfil_tributario' => 'float',
		'scf_cod_cidade' => 'float',
		'scf_perc_multa_atraso' => 'float',
		'scf_sintegra_valores_nf_x929' => 'float',
		'scf_tamanho_loja' => 'float',
		'scf_qtde_colaboradores' => 'float',
		'scf_filial_gerenciada_plugpharma' => 'float',
		'scf_assoc_franquia_plugpharma' => 'int',
		'scf_versao_id' => 'int',
		'scf_inscrita_programa_credito' => 'int',
		'scf_aliq_icms_simples' => 'float',
		'scf_sintegra_valor_60i' => 'int',
		'scf_dt_show_aviso_boleto' => 'int',
		'scf_icms_vda_ecf_nfce_sat' => 'float',
		'scf_csosn_nfce_sat' => 'float',
		'scf_cson_contribuinte_icms' => 'float',
		'scf_cson_nao_contribuinte_icms' => 'float',
		'scf_par_faixa_faturamento' => 'float',
		'scf_csosn_nao_contribuinte_prodvda' => 'float',
		'scf_csosn_contribuinte_prodvda' => 'float'
	];

	protected $dates = [
		'scf_data_proc',
		'scf_ult_backup',
		'scf_prop_dt_nasc',
		'scf_prop_dt_ult_envio',
		'scf_ult_dt_update',
		'date_ins_upd'
	];

	protected $fillable = [
		'scf_razao',
		'scf_fantasia',
		'scf_endereco',
		'scf_cidade',
		'scf_estado',
		'scf_bairro',
		'scf_cep',
		'scf_telefone',
		'scf_fax',
		'scf_cnpj',
		'scf_inscricao',
		'scf_data_proc',
		'scf_juro_mora_mes',
		'scf_senha_liber',
		'scf_senha_liber_cli',
		'scf_senha_liber_rec',
		'scf_senha_liber_estneg',
		'scf_senha_liber_prevda',
		'scf_senha_liber_precpr',
		'scf_senha_liber_trnvda',
		'scf_ult_backup',
		'scf_dt_bl',
		'scf_tipo',
		'scf_cliente',
		'scf_usuario',
		'scf_versao',
		'scf_log_write',
		'scf_log_read',
		'scf_mes_atual',
		'scf_prop_email',
		'scf_prop_dt_nasc',
		'scf_prop_nome',
		'scf_prop_dt_ult_envio',
		'scf_ult_dt_update',
		'scf_versao_indicador',
		'scf_complemento',
		'scf_perc_pis',
		'scf_perc_cofins',
		'scf_senha_liber_rec_jur',
		'scf_endereco_num',
		'scf_inscr_mun',
		'scf_versao_build',
		'scf_enquadramento',
		'scf_perfil_tributario',
		'scf_cod_cidade',
		'scf_email',
		'scf_perc_multa_atraso',
		'scf_sintegra_valores_nf_x929',
		'scf_sintegra_registro_60',
		'scf_senha_liber_desctos',
		'scf_tamanho_loja',
		'scf_qtde_colaboradores',
		'scf_filial_gerenciada_plugpharma',
		'scf_assoc_franquia_plugpharma',
		'scf_versao_id',
		'scf_inscrita_programa_credito',
		'scf_aliq_icms_simples',
		'scf_sintegra_valor_60i',
		'scf_dt_vcto_boleto',
		'scf_dt_show_aviso_boleto',
		'scf_icms_vda_ecf_nfce_sat',
		'scf_csosn_nfce_sat',
		'scf_cson_contribuinte_icms',
		'scf_cson_nao_contribuinte_icms',
		'scf_par_faixa_faturamento',
		'scf_identificacao',
		'scf_csosn_nao_contribuinte_prodvda',
		'scf_csosn_contribuinte_prodvda',
		'scf_cnae',
		'date_ins_upd'
	];

	public function carmots()
	{
		return $this->hasMany(Carmot::class, 'mot_fg');
	}

	public function cpanegs()
	{
		return $this->hasMany(Cpaneg::class, 'neg_fg');
	}

	public function estcad1s()
	{
		return $this->hasMany(Estcad1::class, 'cad_fg');
	}

	public function estdes()
	{
		return $this->hasMany(Estde::class, 'des_fg');
	}

	public function estrom1s()
	{
		return $this->hasMany(Estrom1::class, 'rom_filial_origem');
	}

	public function indimos()
	{
		return $this->hasMany(Indimo::class, 'IMO_FG');
	}

	public function indvlrs()
	{
		return $this->hasMany(Indvlr::class, 'VLR_FG');
	}

	public function ofepars()
	{
		return $this->hasMany(Ofepar::class, 'par_fg');
	}

	public function parcmps()
	{
		return $this->hasMany(Parcmp::class, 'cmp_fg');
	}

	public function scfmsg()
	{
		return $this->hasOne(Scfmsg::class, 'msg_fg');
	}

	public function scfpar()
	{
		return $this->hasOne(Scfpar::class, 'par_fg');
	}

	public function scfram()
	{
		return $this->hasOne(Scfram::class, 'sys_fg');
	}
}
