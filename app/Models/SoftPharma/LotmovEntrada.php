<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\SoftPharma;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LotmovEntrada
 * 
 * @property int $mov_fg
 * @property float $mov_nota
 * @property float $mov_tipo
 * @property string $mov_serie
 * @property float $mov_cod_forn
 * @property float $mov_codigo
 * @property float $mov_seq_in_nf
 * @property float $mov_seq_item_lote
 * @property string $mov_lote
 * @property float $mov_qtde_lote
 * @property Carbon $mov_fabricacao
 * @property Carbon $mov_vcto
 * @property string $mov_cod_agregacao
 * @property string $mov_reg_ms
 * @property float $mov_pmc
 *
 * @package App\Models
 */
class LotmovEntrada extends Model
{
	protected $table = 'lotmov_entrada';
	public $incrementing = false;
	public $timestamps = false;
    protected $connection ='mysqlSoftPharma';
	protected $casts = [
		'mov_fg' => 'int',
		'mov_nota' => 'float',
		'mov_tipo' => 'float',
		'mov_cod_forn' => 'float',
		'mov_codigo' => 'float',
		'mov_seq_in_nf' => 'float',
		'mov_seq_item_lote' => 'float',
		'mov_qtde_lote' => 'float',
		'mov_pmc' => 'float'
	];

	protected $dates = [
		'mov_fabricacao',
		'mov_vcto'
	];

	protected $fillable = [
		'mov_lote',
		'mov_qtde_lote',
		'mov_fabricacao',
		'mov_vcto',
		'mov_cod_agregacao',
		'mov_reg_ms',
		'mov_pmc'
	];
}
