<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductProfit
 * 
 * @property int $id
 * @property int $product_code
 * @property int $amount
 * @property float $profit_margin
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Product $product
 *
 * @package App\Models
 */
class ProductProfit extends Model
{
	protected $table = 'product_profit';

	protected $casts = [
		'product_code' => 'int',
		'amount' => 'int',
		'profit_margin' => 'float'
	];

	protected $fillable = [
		'product_code',
		'amount',
		'profit_margin'
	];

	public function product(){
		return $this->belongsTo(Product::class, 'product_code');
	}
}
