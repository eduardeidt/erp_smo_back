<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TaxProductRetained
 * 
 * @property int $branch_id
 * @property int $product_code
 * @property float $bc_icms_ret
 * @property float $value_icms_ret
 * @property float $value_icms_subst
 * @property float $bc_fcp_ret
 * @property float $value_fcp_ret
 *
 * @package App\Models
 */
class TaxProductRetained extends Model
{
	protected $table = 'tax_product_retained';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'branch_id' => 'int',
		'product_code' => 'int',
		'bc_icms_ret' => 'float',
		'value_icms_ret' => 'float',
		'value_icms_subst' => 'float',
		'bc_fcp_ret' => 'float',
		'value_fcp_ret' => 'float'
	];

	protected $fillable = [
		'bc_icms_ret',
		'value_icms_ret',
		'value_icms_subst',
		'bc_fcp_ret',
		'value_fcp_ret'
	];
}
