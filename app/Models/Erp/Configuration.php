<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Configuration
 * 
 * @property int $id
 * @property float $irrf
 * @property float $irrf_extra
 * @property float $social_contribution
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Configuration extends Model
{
	protected $table = 'configuration';

	protected $casts = [
		'irrf' => 'float',
		'irrf_extra' => 'float',
		'social_contribution' => 'float'
	];

	protected $fillable = [
		'icms1',
		'icms2',
		'icms3',
		'icms4',
		'irrf',
		'irrf_extra',   
		'social_contribution',
        'short_expiration_date',
        'medium_expiration_date',
        'long_expiration_date',
	];
}
