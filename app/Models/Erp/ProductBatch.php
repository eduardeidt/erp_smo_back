<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductBatch
 * 
 * @property int $id
 * @property int $status
 * @property int $branch_id
 * @property int $product_code
 * @property string $batch
 * @property Carbon $due_date
 * @property int $promotion_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @package App\Models
 */
class ProductBatch extends Model
{
	protected $table = 'product_batch';

	protected $casts = [
		'branch_id' => 'int',
		'status' => 'int',
		'product_code' => 'int',
		'promotion_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];


	protected $fillable = [
		'branch_id',
		'product_code',
		'batch',
		'due_date',
		'amount',
        'status',
        'reg_ms',
		'created_by',
		'updated_by'
	];
}
