<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GuiaDaFarmacium
 * 
 * @property int $id
 * @property int $product_code
 * @property float $factory_price_17
 * @property float $max_price_17
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Product $product
 *
 * @package App\Models
 */
class GuiaDaFarmacia extends Model
{
	protected $table = 'guia_da_farmacia';

	protected $fillable = [
		'product_code',
		'factory_price_17',
		'max_price_17',
        'apply_fraction',
	];

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_code');
	}
}
