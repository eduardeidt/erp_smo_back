<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductBar
 * 
 * @property int $product_code
 * @property string $bar_code
 * 
 * @property Product $product
 *
 * @package App\Models
 */
class ProductBar extends Model
{
	protected $table = 'product_bar';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'product_code' => 'int'
	];

	protected $fillable = [
		'product_code',
		'bar_code'
	];

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_code');
	}
}
