<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductPromotionHistory
 * 
 * @property int $id
 * @property int $product_code
 * @property int $promotion_id
 * @property Carbon $sell_value
 * @property Carbon $promotion_rule
 * @property Carbon $end_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @package App\Models
 */
class ProductPromotionHistory extends Model
{
	protected $table = 'product_promotion_history';

	protected $casts = [
		'product_code' => 'int',
		'promotion_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'product_code',
		'promotion_id',
		'sell_value',
		'promotion_rule',
		'begin_date',
		'end_date',
		'created_by',
		'updated_by'
	];
}
