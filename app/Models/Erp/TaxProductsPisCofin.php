<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TaxProductsPisCofin
 * 
 * @property int $product_code
 * @property Carbon $begin_date
 * @property int $tax_type
 * @property int $code_sped
 * @property float $perc_pis_lucroreal
 * @property float $perc_cofins_lucroreal
 * @property float $perc_pis_presumido
 * @property float $perc_cofins_presumido
 * @property Carbon $change_date
 * @property string $user
 * @property Carbon $date_proc_automatico
 *
 * @package App\Models
 */
class TaxProductsPisCofin extends Model
{
	protected $table = 'tax_product_pis_cofins';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'product_code' => 'int',
		'tax_type' => 'int',
		'code_sped' => 'int',
		'perc_pis_lucroreal' => 'float',
		'perc_cofins_lucroreal' => 'float',
		'perc_pis_presumido' => 'float',
		'perc_cofins_presumido' => 'float'
	];

	protected $dates = [
		'begin_date',
		'change_date',
		'date_proc_automatico'
	];

	protected $fillable = [
		'tax_type',
		'code_sped',
		'perc_pis_lucroreal',
		'perc_cofins_lucroreal',
		'perc_pis_presumido',
		'perc_cofins_presumido',
		'change_date',
		'user',
		'date_proc_automatico'
	];
}
