<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Substance
 * 
 * @property int $code
 * @property int $type
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|Product[] $products
 *
 * @package App\Models
 */
class Substance extends Model
{
	protected $table = 'substance';
	public $incrementing = false;
    public $primaryKey = 'code';

	protected $casts = [
		'code' => 'int',
		'type' => 'int'
	];

	protected $fillable = [
        'code',
        'type',
		'name'
	];

	public function products()
	{
		return $this->belongsToMany(Product::class, 'product_substance', 'substance_code', 'product_code')
			->withPivot('substance_type')
			->withTimestamps();
	}
}
