<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Traits\HasCompositePrimaryKey;

/**
 * Class ProductExtraObserver
 *
 * @property int $id
 * @property int $product_code
 * @property int $stock
 * @property int $branch_id
 * @property string $abc_curve
 * @property float $sell_value
 * @property float $cost_price
 * @property float $medium_price
 * @property float $profit
 * @property string $promotion_rule
 * @property Carbon $last_purchase
 * @property Carbon $last_sale
 * @property Carbon $last_price_update
 * @property Carbon $manual_expiration_date
 *
 * @package App\Models
 */
class ProductExtra extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'product_extra';
    public $timestamps = false;
    public $primaryKey = ['product_code', 'branch_id'];

    protected $fillable = [
        'product_code',
        'stock',
        'branch_id',
        'abc_curve',
        'sell_value',
        'cost_price',
        'medium_cost',
        'accounting_cost',
        'profit',
        'promotion_rule',
        'commission',
        'last_purchase',
        'last_sale',
        'last_price_update',
        'manual_expiration_date'
    ];

    public function branch(){
        return $this->belongsTo(Branch::class);
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_code');
    }
}
