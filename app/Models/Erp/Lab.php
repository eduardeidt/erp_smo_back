<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Lab
 * 
 * @property int $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Lab extends Model
{
	protected $table = 'lab';

	protected $fillable = [
		'name',
		'active'
	];

    public function getActiveLabs() {
        return $this->select('id','name')->where('active' , '1')->get();
    }
}
