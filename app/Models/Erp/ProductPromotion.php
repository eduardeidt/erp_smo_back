<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductPromotion
 *
 * @property int $id
 * @property int $promotion_id
 * @property int $product_code
 * @property float $sell_value
 * @property string $promotion_rule
 * @property bool $active
 * @property bool $is_checked
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $created_by
 * @property Carbon $updated_by
 *
 * @property Promotion $promotion
 * @property Product $product
 *
 * @package App\Models
 */
class ProductPromotion extends Model
{
	protected $table = 'product_promotion';

	protected $casts = [
		'promotion_id' => 'int',
		'product_code' => 'int',
		'active' => 'bool',
		'is_checked' => 'bool'
	];

	protected $fillable = [
		'promotion_id',
		'product_code',
		'sell_value',
		'promotion_rule',
        'sell_value',
		'status',
		'is_checked',
        'alter_sell_value',
	];

	public function promotion()
	{
		return $this->belongsTo(Promotion::class);
	}

	public function product(){
		return $this->belongsTo(Product::class, 'product_code');
	}
	public static function getRuleByPromotionAndProduct($promotionId,$productCode): string{
	    return ProductPromotion::firstWhere(['promotion_id'=>$promotionId,'product_code'=>$productCode])->promotion_rule;
    }

}
