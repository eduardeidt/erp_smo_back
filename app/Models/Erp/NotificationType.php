<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NotificationType
 *
 * @property int $id
 * @property string $description
 * @property bool $system
 * @property bool $active
 *
 * @property Collection|Notification[] $notifications
 *
 * @package App\Models
 */
class NotificationType extends Model
{
	protected $table = 'notification_type';
	public $timestamps = false;

	protected $casts = [
		'system' => 'bool',
		'active' => 'bool'
	];

	protected $fillable = [
		'description',
		'system',
		'active'
	];

	public function notifications()
	{
		return $this->hasMany(Notification::class);
	}
	public static function getTitle($id){
	    return NotificationType::find($id)->description;
    }
}
