<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductBatchProductPromotion
 * 
 * @property int $id
 * @property int $product_promotion_id
 * @property int $product_batch
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property ProductPromotion $product_promotion
 *
 * @package App\Models
 */
class ProductBatchPromotion extends Model
{
	protected $table = 'product_batch_promotion';

	protected $casts = [
		'product_promotion_id' => 'int',
		'product_batch' => 'int',
		'created_at' => 'int',
		'updated_at' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'product_promotion_id',
		'product_batch',
		'created_by',
		'updated_by'
	];

	public function product_promotion()
	{
		return $this->belongsTo(ProductPromotion::class);
	}
}
