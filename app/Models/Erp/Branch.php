<?php

namespace App\Models\Erp;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Branch
 *
 * @property int $id
 * @property string $description
 * @property string $city
 * @property string $name
 *
 * @package App\Models
 */
class Branch extends Model
{
	protected $table = 'branch';

	protected $fillable = [
		'description',
		'city',
		'name'
	];

	public static function getBranchs(){
	    return Branch::all();
    }
}
