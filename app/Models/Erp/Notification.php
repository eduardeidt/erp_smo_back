<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 *
 * @property int $id
 * @property int $product_code
 * @property int $product_main_code
 * @property int $user_id
 * @property int $notification_type_id
 * @property bool $status
 * @property int $branch_ids
 * @property string $title
 * @property Carbon $date_limit
 * @property bool $appelant
 * @property string $observation
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Product $product
 * @property User $user
 * @property NotificationType $notification_type
 *
 * @package App\Models
 */
class Notification extends Model
{
	protected $table = 'notification';

	protected $casts = [
		'product_code' => 'int',
		'product_main_code' => 'int',
		'user_id' => 'int',
		'notification_type_id' => 'int',
		'status' => 'bool',
		'branch_id' => 'int',
		'appelant' => 'bool'
	];


	protected $fillable = [
		'product_code',
		'product_main_code',
		'user_id',
		'notification_type_id',
		'status',
		'branch_id',
		'title',
		'date_limit',
		'appelant',
		'observation'
	];

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_main_code');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function notification_type()
	{
		return $this->belongsTo(NotificationType::class);
	}
}
