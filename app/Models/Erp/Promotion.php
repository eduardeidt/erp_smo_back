<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use App\Traits\Trackable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotion
 *
 * @property int $id
 * @property string $name
 * @property Carbon $begin_date
 * @property Carbon $end_date
 * @property bool $status
 * @property string $day_of_week
 * @property bool $priority
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $created_by
 * @property Carbon $updated_by
 *
 * @property Collection|Product[] $products
 *
 * @package App\Models
 */
class Promotion extends Model
{
    use Trackable;
    protected $table = 'promotion';
    protected $fillable = [
        'name',
        'begin_date',
        'end_date',
        'status',
        'day_of_week',
        'priority',
        'created_by',
        'updated_by'
    ];

    public function products(){
        return $this->belongsTo(Product::class, 'product_main_code');
    }
    public static function get_active_promotions(){
        return Promotion::where('status',1)->get();
    }
}
