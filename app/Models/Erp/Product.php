<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Arr;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Product
 *
 * @property int $code
 * @property string $bar_code
 * @property int $group_id
 * @property float $type_id
 * @property string $lab
 * @property string $description
 * @property bool $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @package App\Models
 */
class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'code';
    protected $casts = [
        'diff_between_costs'=>'float',
    ];
    protected $fillable = [
        'code',
        'bar_code',
        'product_code_main',
        'group_id',
        'type_id',
        'lab_id',
        'promotion_id',
        'old_promotion_id',
        'description',
        'presentation',
        'active',
        'last_purchase',
        'code_origin',
        'fraction'
    ];

    public function notifications(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(Notification::class, 'product_main_code');
    }

    public function product_extras(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(ProductExtra::class, 'product_code');
    }

    public function tax_products(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(TaxProduct::class, 'product_code','code');
    }

    public function promotions(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(Promotion::class, 'id', 'promotion_id');
    }
    public function tax_products_pis_cofins(): \Illuminate\Database\Eloquent\Relations\HasMany {
        return $this->hasMany(TaxProductsPisCofin::class, 'product_code');
    }
    public static function getProductsByMainCode($code){
        $productMain = Product::where('code',$code)->first()->product_code_main;
        if (!is_null($productMain)){
            return Product::select('code')->where('product_code_main',$productMain)->pluck('code');
        }
        return [];
    }
    public static function getMainCodeByProduct($code){
        return Product::select('product_code_main')
                ->where('code',$code)
                ->whereNotNull('product_code_main')
                ->first()->product_code_main ?? NULL;
    }

    public static function getProducts($request){
        $operatorWhenStock = $request->stock == 1 ? '>': '<=';
        $operatorWhenInPromotion = $request->in_promotion == 1 ? '<>': '=';
        $methodNeverSold = $request->never_sold == 1 ? 'whereNull': 'whereNotNull';

        $productSql = Product::with('product_extras')
            ->select( 'product.code','product.bar_code','product.product_code_main','promotion_id','group_id','type_id','lab.name as lab_name','description',
                'product.active','last_purchase','code_origin','p.name AS promotion_name','product.presentation','accounting_cost','last_price_update',
                DB::raw('round(medium_cost * 100 / accounting_cost -100,2) as diff_between_costs')
            )
            ->join('lab','lab_id','id')
            ->leftJoin('promotion AS p','p.id','product.promotion_id')
            ->join('product_extra as pe',function($q) use ($operatorWhenStock, $request) {
                $q->on('pe.product_code','product.code')
                    ->when(!is_null($request->stock), function ($w) use ($operatorWhenStock) {
                        $w->where("stock",$operatorWhenStock, 0);
                    });
            });
        /**
         * Check Product By description
         **/
        if (!is_null($request->description)) {
            if (is_numeric($request->description)) {
                $productSql->leftjoin('product_bar as pb', 'pb.product_code', 'product.code')
                    ->where(function ($q) use ($request) {
                        $q->where('product.bar_code', $request->description)
                            ->orWhere('pb.bar_code', $request->description);
                    });
                $productSql->limit(1);
            } else {
                $productSql->whereRaw("concat(description, ' ', presentation) like '%{$request->description}%'");
            }
        }
        /**
         * Check if Product in promotion
         */
        if (!is_null($request->in_promotion)) {
            $productSql->where('promotion_rule', $operatorWhenInPromotion, '');
        }
        /**
         * Check if Product was never Sold
         */
        if (!is_null($request->never_sold)) {
            $productSql->$methodNeverSold('last_sale');
        }
        /**
         * If filter Lab
         */
        if (!empty($request->dataLab)) {
            $productSql->whereIn("lab_id", $request->dataLab);
        }
        /**
         * if filter by Type
         */
        if (!empty($request->dataType)) {
            $productSql->whereIn("type_id", $request->dataType);
        }
        /**
         * if filter by Group
         */
        if (!empty($request->dataGroup)) {
            $productSql->whereIn("group_id", $request->dataGroup);
        }
        /**
         * if filter by Promotion
         */
        if (!empty($request->promotion_id)) {
            $productSql->whereIn("promotion_id", $request->promotion_id);
        }
        /*
         * Filter By Profit
         */
        if (!empty($request->minProfit)) {
            $productSql->join('product_profit AS pf',function ($q) use ($request) {
                $q->on('pf.product_code','product.code')
                    ->where('profit_margin','>=',$request->minProfit)
                    ->where('profit_margin','<=',$request->maxProfit);
            });
        }
        if (!empty($request->dataSubstance)) {
            $productSql->join('product_substance AS ps','ps.product_code','product.code')
                ->join('substance AS s',function ($q) use ($request) {
                    $q->on('ps.substance_code','s.code')
                        ->where('ps.substance_type','s.type')
                        ->whereIn('s.id',$request->dataSubstance);
                });
        }
        if (!empty($request->dataPharmacological)) {
            $productSql->join('product_substance AS pss','pss.product_code','product.code')
                ->join('substance AS ss',function ($q) use ($request) {
                    $q->on('pss.substance_code','ss.code')
                        ->where('pss.substance_type','ss.type')
                        ->whereIn('ss.id',$request->dataPharmacological);
                });
        }
        if (!empty($request->dataTherapeutic)) {
            $productSql->join('product_substance AS psss','psss.product_code','product.code')
                ->join('substance AS sss',function ($q) use ($request) {
                    $q->on('psss.substancE_code','sss.code')
                        ->where('psss.substance_type','sss.type')
                        ->whereIn('sss.id',$request->dataTherapeutic);
                });
        }
        if (!is_null($request->groupedProducts)){
            $productSql->whereIn('code',self::getProductsByMainCode($request->productCode));
        }
        $productSql->where("product.active", "1")
            ->groupBy('product.code');
        $productSql->orderBy('description');
        $productSql->orderBy('presentation');

//        return $productSql->get();
        return $productSql->paginate(50);

    }
    public static function getByBarCode($barCode){
        return Product::leftjoin('product_bar as pb', 'pb.product_code', 'product.code')
            ->where(function ($q) use ($barCode) {
                $q->where('product.bar_code', $barCode)
                    ->orWhere('pb.bar_code', $barCode);
            })->first();
    }
}
