<?php

namespace App\Models\Erp;

use App\Traits\Trackable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $branch_ids
 * @property int $active
 * @property int $admin
 *
 * @property Collection|Company[] $companies
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
    use Trackable;

    protected $table = 'user';
    protected $hidden = ['password'];
    public $timestamps = false;
    protected $fillable = [
        'user_type_id',
        'name',
        'user',
        'email',
        'password',
        'admin',
        'active',
        'branch_ids',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
