<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductSubstance
 * 
 * @property int $product_code
 * @property int $substance_code
 * @property int $substance_type
 * @property int $created_at
 * @property int $updated_at
 * 
 * @property Product $product
 * @property Substance $substance
 *
 * @package App\Models
 */
class ProductSubstance extends Model
{
	protected $table = 'product_substance';
	public $incrementing = false;

	protected $casts = [
		'product_code' => 'int',
		'substance_code' => 'int',
		'substance_type' => 'int',
		'created_at' => 'int',
		'updated_at' => 'int'
	];

	protected $fillable = [
		'product_code',
		'substance_code',
		'substance_type'
	];

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_code');
	}

	public function substancy()
	{
		return $this->belongsTo(Substance::class, 'substance_code');
	}
}
