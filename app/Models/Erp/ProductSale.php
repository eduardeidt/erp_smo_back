<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductSale
 * 
 * @property Carbon $date
 * @property int $branch_id
 * @property int $seller_id
 * @property int $product_code
 * @property float $total_value
 * @property float $discount_value
 * @property float $net_value
 * @property float $value_cmv
 * @property int $amount
 *
 * @package App\Models
 */
class ProductSale extends Model
{
	protected $table = 'product_sale';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'branch_id' => 'int',
		'seller_id' => 'int',
		'product_code' => 'int',
		'total_value' => 'float',
		'discount_value' => 'float',
		'net_value' => 'float',
		'value_cmv' => 'float',
		'amount' => 'int'
	];

	protected $fillable = [
		'date',
		'branch_id',
		'seller_id',
		'product_code',
		'total_value',
		'discount_value',
		'net_value',
		'value_cmv',
		'amount'
	];
}
