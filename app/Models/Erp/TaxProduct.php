<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Erp;
use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TaxProduct
 *
 * @property int $product_code
 * @property string $uf
 * @property int $tipo_tributacao
 * @property float $aliq_icms
 * @property float $fundo_probreza
 * @property float $pmc
 * @property float $mva
 * @property int $branch
 * @property float $perc_red_bc_icms
 * @property float $perc_red_bc_icms_st
 * @property float $aliq_iss
 * @property string $iss_cod_mun
 * @property string $iss_atividade
 * @property int $iss_natureza
 * @property string $iss_cnae
 * @property float $aliq_ecf
 * @property Carbon $alteracao
 * @property string $alteracao_usuario
 * @property string $cod_beneficio
 * @property float $aliq_icms_desonerado
 * @property float $fundo_probreza_desonerado
 *
 * @package App\Models
 */
class TaxProduct extends Model
{
    use HasCompositePrimaryKey;
	protected $table = 'tax_product';
	public $incrementing = false;
	public $timestamps = false;
    protected $primaryKey = ['product_code ','uf','branch'];

	protected $fillable = [
        'product_code',
        'uf',
		'tipo_tributacao',
		'aliq_icms',
		'fundo_probreza',
		'pmc',
		'mva',
		'perc_red_bc_icms',
		'perc_red_bc_icms_st',
		'aliq_iss',
		'iss_cod_mun',
		'iss_atividade',
		'iss_natureza',
		'iss_cnae',
		'aliq_ecf',
		'alteracao',
		'alteracao_usuario',
		'cod_beneficio',
		'aliq_icms_desonerado',
		'fundo_probreza_desonerado'
	];

    public function product(){
        return $this->belongsTo(Product::class, 'product_code');
    }
}
