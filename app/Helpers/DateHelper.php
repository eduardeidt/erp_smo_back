<?php


namespace App\Helpers;

use Carbon\Carbon;
use DateTime;

class DateHelper
{
    public static function day_week_br($day_of_week, $short = null, $complete = null)
    {
        if ($complete) {
            $day_week = array(
                'Sunday' => 'Domingo',
                'Monday' => 'Segunda-feira',
                'Tuesday' => 'Terça-feira',
                'Wednesday' => 'Quarta-feira',
                'Thursday' => 'Quinta-feira',
                'Friday' => 'Sexta-feira',
                'Saturday' => 'Sábado'
            );
            return $day_week[$day_of_week];
        }
        if (!$short) {
            $day_week = array(
                '0' => 'Domingo',
                '1' => 'Segunda-feira',
                '2' => 'Terça-feira',
                '3' => 'Quarta-feira',
                '4' => 'Quinta-feira',
                '5' => 'Sexta-feira',
                '6' => 'Sábado',
                '7' => 'Domingo',
            );
            return $day_week[$day_of_week];
        } else {
            $day_week = array(
                '0' => 'Dom',
                '1' => 'Seg',
                '2' => 'Ter',
                '3' => 'Qua',
                '4' => 'Qui',
                '5' => 'Sex',
                '6' => 'Sáb',
                '7' => 'Dom',
            );
            return $day_week[$day_of_week];
        }
    }

    public static function format_date_english($date)
    {
        if (!$date) {
            return NULL;
        }
        $date = implode('-', array_reverse(explode('/', $date)));
        $new_date = new DateTime($date);
        return $new_date->format('Y-m-d');
    }

    public static function format_date($date, $format){
        if (!$date) {
            return NULL;
        }
        $date = implode('-', array_reverse(explode('/', $date)));
        $new_date = new DateTime($date);
        return $new_date->format($format);
    }

    public static function format_date_pt_br($date)
    {
        $monthEnUs = array(
            '1' => "/January/",
            '2' => "/February/",
            '3' => "/March/",
            '4' => "/April/",
            '5' => "/May/",
            '6' => "/June/",
            '7' => "/July/",
            '8' => "/August/",
            '9' => "/September/",
            '10' => "/October/",
            '11' => "/November/",
            '12' => "/December/"
        );
        $monthPtBR = array(
            '1' => "Janeiro",
            '2' => "Fevereiro",
            '3' => "Março",
            '4' => "Abril",
            '5' => "Maio",
            '6' => "Junho",
            '7' => "Julho",
            '8' => "Agosto",
            '9' => "Setembro",
            '10' => "Outubro",
            '11' => "Novembro",
            '12' => "Dezembro"
        );
        $new_date = new DateTime($date);
        $day_week = self::day_week_br($new_date->format('l'), null, true);
        $date = $new_date->format('j \d\e F \d\e Y');
        $date = preg_replace($monthEnUs, $monthPtBR, $date);
        return "{$day_week}, {$date}";
    }
    public static function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d'){
        $dates = [];
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }

    public static function add_sub_days($date, $add_sub, $days, $format = 'Y-m-d')
    {
        $dateCarbon = new Carbon($date);
        return $dateCarbon->$add_sub($days, 'day')->format($format);
    }
}
