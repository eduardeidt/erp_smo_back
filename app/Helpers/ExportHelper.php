<?php


namespace App\Helpers;

class ExportHelper
{
    public static function writeNumber(string $content, int $size, string $padString = "0")
    {
        $content = trim($content);

        if ($size < strlen($content)) {
            return substr($content, 0, $size);
        }

        return str_pad($content, $size, $padString, STR_PAD_LEFT);
    }
    public static function writeEan(string $content, int $size, string $padString = "0")
    {
        $content = trim($content);

        if (strlen($content ==13)) {
            return substr($content, 0, $size);
        }

        return str_pad($content, $size, $padString, STR_PAD_LEFT);
    }

    /**
     * @param string $content String to write
     * @param int $size Total size to pad
     * @param string $padString String to pad
     *
     * @return string
     */
    public static function writeString(string $content, int $size, string $padString = " ")
    {
        $content = trim($content);

        if ($size < strlen($content)) {
            return substr($content, 0, $size);
        }

        return str_pad($content, $size, $padString,STR_PAD_RIGHT);
    }
    public static function clear(?string $content)
    {
        return str_replace(['.', '/', '-', ':',','], "", $content);
    }

}
