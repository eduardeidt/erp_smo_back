<?php


    namespace App\Helpers;

    class MessagesHelper{
        /**
         * function to return response message
         *
         * @param $condition: success or error
         * @param $type_message:  message to show on front
         */
        public static function messages($condition,$type_message){
            $array_message = array(
                'success' => array(
                    'create' => ['msg' =>'Criado com sucesso','status' =>201],
                    'update' => ['msg' =>'Atualizado com sucesso','status' =>200],
                    'delete' => ['msg' =>'Excluído com sucesso','status' =>200],
                    'created' => ['msg' =>'Criado com sucesso','status' =>201],
                    'updated' => ['msg' =>'Atualizado com sucesso','status' =>200],
                    'deleted' => ['msg' =>'Excluído com sucesso','status' =>200],
                    'synced' => ['msg' =>'Produtos atualizados com sucesso','status' =>200],
                ),
                'error' => array(
                    'generic' => ['error'=>['msg' =>'Ops! algo de errado aconteu. Entre contato conosco e conte-nos o que estava fazendo','status' =>403]],
                    'empty' => ['error'=>['msg' =>"Dados não encontrados",'status' =>404]],
                    'login_fail' => ['error'=>['msg' =>"Usuário ou senha inválidos",'status' =>401]],
                    'lowest_priority_promotion' => ['error'=>['msg' =>"Promoção com prioridade inferior a atual. Nada Será alterado",'status' =>403]],
                    'bar_code_invalid' => ['error'=>['msg' =>"Código de barras inválido",'status' =>403]],
                )
            );
            return $array_message[$condition][$type_message];
        }

    }
