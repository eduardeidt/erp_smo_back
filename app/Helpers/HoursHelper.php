<?php


namespace App\Helpers;

use Carbon\Carbon;
use DateTime;

class HoursHelper{
    public static function calc_times($time1, $time2, $add_sub){
        if ($add_sub == 'add') {
            $times = array($time1, $time2);
            $seconds = 0;
            foreach ($times as $time) {
                $hour = 0;
                $minute = 0;
                if (sizeof(explode(':', $time)) == 2) {
                    list($hour, $minute) = explode(':', $time);
                }
                $seconds += $hour * 3600;
                $seconds += $minute * 60;
            }
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
            $minutes = floor($seconds / 60);
            return sprintf('%02d:%02d', $hours, $minutes);

        } elseif ($add_sub == 'sub') {
            if ($time1 < $time2) {
                $signal = "-";
            } else {
                $signal = '';
            }
            $startTime = new DateTime($time1);
            $endTime = new DateTime($time2);
            $duration = $startTime->diff($endTime);
            return $duration->format("{$signal}%H:%I");
        }
    }
    public static function calc_timesAbsolute($time1, $time2, $add_sub){
        if ($add_sub == 'add') {
            $times = array($time1, $time2);
            $seconds = 0;
            foreach ($times as $time) {
                $hour = 0;
                $minute = 0;
                if (sizeof(explode(':', $time)) == 2) {
                    list($hour, $minute) = explode(':', $time);
                }
                $seconds += $hour * 3600;
                $seconds += $minute * 60;
            }
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
            $minutes = floor($seconds / 60);
            return sprintf('%02d:%02d', $hours, $minutes);

        } elseif ($add_sub == 'sub') {
            $startTime = new DateTime($time1);
            $endTime = new DateTime($time2);
            $duration = $startTime->diff($endTime);
            return $duration->format("%H:%I");
        }
    }

    /*
     * time 1 - time2
     */
    public static function difTimes($dtime,$atime){
        if ($atime =='00:00'){
            return $dtime;
        }
        $dep = explode(':',$dtime);
        $arr = explode(':',$atime);
        $diff = abs(mktime($dep[0],$dep[1],0,date('n'),date('j'),date('y'))-mktime($arr[0],$arr[1],0,date('n'),date('j'),date('y')));
        $hours = floor($diff/(60*60));
        $mins = floor(($diff-($hours*60*60))/(60));
        $secs = floor(($diff-(($hours*60*60)+($mins*60))));
        if(strlen($hours)<2){$hours="0".$hours;}
        if(strlen($mins)<2){$mins="0".$mins;}
        if(strlen($secs)<2){$secs="0".$secs;}
        if (self::hours_to_minute($dtime) < self::hours_to_minute($atime)) {
            $signal = "-";
        } else {
            $signal = '';
        }
        return $signal.$hours.':'.$mins;
    }
    public static function calc_times_carbon($time1, $time2, $journey = false)
    {
        $startTime = new Carbon($time1);
        $endTime = new Carbon($time2);
        if ($startTime->lte($endTime) && $journey) {
            $startTime->addDay();
        }
        $h = $startTime->diffInHours($endTime);
        $m = $startTime->diff($endTime)->format("%I");
        return "{$h}:{$m}";
    }
    public static function now(){
        $hour = new DateTime();
        return $hour->format('H:i:s');
    }
    public static function time_from_seconds($seconds) {
        $h = floor($seconds / 3600);
        $m = floor(($seconds % 3600) / 60);
        return sprintf('%02d:%02d', $h, $m);
    }
    public static function calc_hours($hour1,$hour2){
        $si1 = $hour1[0];
        $si2 = $hour2[0];
        $hour1 = substr($hour1,1,8);
        $hour2 = substr($hour2,1,8);

        $secondes1=intval($si1.heure_to_secondes($hour1));
        $secondes2=intval($si2.heure_to_secondes($hour2));

        $somme=intval($secondes1+$secondes2);

        //transfo en h:i:s
        $s= ($somme % 60);
        $m1=   (($somme-$s) / 60);
        $m= ($m1 % 60);
        $h= (($m1-$m) / 60);

        if($somme > 0) { $sif = '+'; }
        $resultat=sprintf("%02d", $h).":".sprintf("%02d", abs($m)).":".sprintf("%02d", abs($s))."";
        return  $sif.$resultat;
    }

    public static function heure_to_secondes($heure){
        $array_heure=explode(":",$heure);
        $secondes=3600*$array_heure[0]+60*$array_heure[1]+$array_heure[2];
        return $secondes;
    }

    public static function hours_to_minute($hour){
        $arr = explode(':',$hour);
        return $arr[0] * 60 + $arr[1];
    }
}
