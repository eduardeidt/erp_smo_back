<?php

namespace App\Helpers;

use App\Models\Replic;
use Illuminate\Support\Facades\DB;

class SoftpharmaHelper{

    public static function updateSoftpharma($table,$data,$where){
        DB::setDefaultConnection('mysqlSoftPharma');
        DB::enableQueryLog();
        DB::table($table)->where($where)->update($data);
        self::insert_replic();
        DB::setDefaultConnection('mysql');
    }
    protected static function insert_replic(){
        Replic::create(array(
            'SQL' =>self::get_last_sql(),
            'idFilial'=>1,
            'DataMvto'=> now()->toDateTimeString(),
            'NivelUrgencia' =>1,
            'processada'=> 0
        ));
    }
    /*
         make sure this function above:
         ---- DB::enableQueryLog() -----
        was enabled before execute the query
         */
    protected static function get_last_sql(){
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        $last_query = self::bind_data_to_query($last_query);
        return $last_query;
    }
    protected static function bind_data_to_query($queryItem){
        $query = $queryItem['query'];
        $bindings = $queryItem['bindings'];
        $arr = explode('?',$query);
        $res = '';
        foreach($arr as $idx => $ele){
            if($idx < count($arr) - 1){
                $res = $res.$ele."'".$bindings[$idx]."'";
            }
        }
        $res = $res.$arr[count($arr) -1];
        return $res;
    }
    public static function get_origin_product($code = null){
        $arrayOriginProduct = array(
            0=>'NACIONAL, EXCETO AS INDICAÇÕES NOS CODIGO 3,4,5',
            1=>'EXTRANGEIRA - IMPORTAÇÃO DIRETA, EXCETO A INDIC',
            2=>'EXTRANGEIRA - ADQUIRIDA NO MERCADO INTERNO',
            3=>'NACIONAL, MERCADORIA OU BEM COM CONTEÚDO',
            4=>'NACIONAL, CUJA PRODUCAO TENHA SIDO FEITA EM ',
            5=>'NACIONAL, MERCADORI OU BEM COM CONTEÚDO',
            6=>'ESTRANGEIRO - IMPORTACAO DIRETA, SEM SIMILAR',
            7=>'ESTRANGEIRA - ADQUIRIDA NO MERCADO INTERNO',
            8=>'NACIONAO, MERCADORIA OU BEM COM CONTEÚDO',
        );
        if ($code){
            return $arrayOriginProduct[$code];
        }else{
            $arrayOriginProduct;
        }
    }

    /**
     * @param $tax_type; //table tax_product_pis_cofins.tax_type
     * @return string
     */
    public static function get_type_tax($tax_type)
    {
        $taxs = array(
            0 => 'Tributado',
            1 => 'Monofasico',
            3 => 'Aliquita 0%',
            4 => 'Isento 7',
        );
        return $taxs[$tax_type];
    }
    /**
     * @param $tax_type; //table tax_product.tipo_tributacao
     * @return string
     */
    public static function tributacao($tax_type){
        $taxs = array(
            0=>'Tributado',
            1=>'Substituição',
            2=>'Isento',
            3=>'Não Tributado',
        );
        return $taxs[$tax_type];

    }
    public static function getGroups() {
        return array(
            ['id'=>0 , 'name' =>'Medicamentos'],
            ['id'=>1 ,'name'=> 'Perfumaria'],
            ['id'=>2 ,'name'=> 'Psicotrópicos'],
            ['id'=>3 ,'name'=> 'Conveniência'],
            ['id'=>5 ,'name'=> 'Éticos'],
            ['id'=>6 ,'name'=> 'Similares'],
            ['id'=>7 ,'name'=> 'Genéricos'],
            ['id'=>10 ,'name'=> 'Especial1'],
            ['id'=>17 ,'name'=> 'Psicotrópicos Genéricos'],
            ['id'=>18 ,'name'=> 'Combustível']
        );
    }
}
