<?php


namespace App\Helpers;

use App\Models\Erp\ProductPromotionHistory;

class PromotionHelper{

    public static function storeProductPromotionHistory(
        $productCode,
        $promotionId,
        $sellValue,
        $promotionRule,
        $beginDate,
        $endDate){

        ProductPromotionHistory::create([
            'product_code'=>$productCode,
            'promotion_id'=>$promotionId,
            'sell_value'=>$sellValue,
            'promotion_rule'=>$promotionRule,
            'begin_date'=>$beginDate,
            'end_date'=>$endDate,
        ]);
    }
    public static function updateProductPromotionHistory($productCode,$promotionId,$endDate){
        ProductPromotionHistory::where('product_code',$productCode)
            ->where('promotion_id',$promotionId)
            ->whereNull('end_date')
            ->update(['end_date'=>$endDate]);
    }
}
