<?php


namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Helper
{
    /**
     * @param array $data
     * @param $current_price
     * @return string
     */
    public static function calc_percentage_off($data,  string $current_price=null): string
    {
        $returnString = '';
        if (!count($data)>0 or $data == ''){  return $returnString;}
        foreach ($data as $amount => $new_price) {
            $percentage_off = 100 - ($new_price * 100 / $current_price);
            $percentage_off = round($percentage_off, 2);
            $percentage_off = $percentage_off * 100;
            $percentage_off = explode(".", $percentage_off);
            $percentage_off = $percentage_off[0];
            $percentage_off = str_pad($percentage_off, 5, 0, STR_PAD_LEFT);
            $amount = str_pad($amount, 3, 0, STR_PAD_LEFT);
            $returnString .= "{$amount}>={$percentage_off}";
        }
        return $returnString;
    }

    public static function calc_value_off(string $promotionRule, $sellValue): array
    {
        $arrayReturn = [];
        if ($promotionRule <> '' and $sellValue <> '') {
            $promotionRule = str_split(trim($promotionRule), 10);
            foreach ($promotionRule as $rule) {
                $auxRule = explode('>=', $rule);
                $amount = (int)$auxRule[0];
                $perc = (int)substr($auxRule[1], 0, 3) . '.' . substr($auxRule[1], 3);
                $off_value = ($auxRule[1] / 100) * $sellValue / 100;
                $price = number_format($sellValue - $off_value, 2,'.','');
                $arrayReturn[] = [
                    'amount' => $amount,
                    'price' => $price,
                    'perc' => $perc,
                ];
            }
        }
        return $arrayReturn;
    }

    public static function cal_tax_product($icms, $credit, $sell_price, $contribSocial, $irrf, $extraIrrf, $percPis, $percCofins,
                                           $mediumCost, $taxCode, $commission, $fix_cost, $taxType): array {
        /**
         * Se for importado ou fora do Estado o credito é 4%
         *1,2,5,6 tem 4 %
         * MONOFASICO NAO TEM PIS/COFINS, TANTO EM 25% OU 17%
         */
        $ir = $sell_price * $irrf / 100;
        $extraIrrfCalc = $sell_price * $extraIrrf / 100;
        $irrfCalc = $extraIrrfCalc + $ir;
        $arrayCheck1 = ['0', '3'];
        if (in_array($taxCode,$arrayCheck1)){
            $percCofins = $sell_price * $percCofins / 100;
            $percPis = $sell_price * $percPis / 100;
        }
        $icmsValue = 0;
        $arrayCheck2 =['1', '2','3'];
        if (!in_array($taxType,$arrayCheck2)){
            $creditValue = $mediumCost * ((100 - $credit) / 100);
            $icmsValue = $sell_price * $icms / 100;
        }else{
            $creditValue = $mediumCost;
        }

        $contribSocial = $sell_price * $contribSocial / 100;
        $custoVariavel = $creditValue;
        $cost = ($icmsValue + $contribSocial + $irrfCalc + $percPis + $percCofins + $creditValue) + ($sell_price * $commission /100);
        $costTax = $icmsValue + $contribSocial + $irrfCalc + $percPis + $percCofins;
        $margemLiquida = $sell_price * 100 / $cost - 100;
        $receitaLiquida = $sell_price - $costTax;
        $lucroLiquidoValor = $receitaLiquida - $creditValue;
        $lucroLiquidoPerc = $lucroLiquidoValor / $sell_price * 100;
        return [
            'icmsValue' => number_format($icmsValue,2,'.',''),
            'creditValue' => number_format($creditValue,2,'.',''),
            'contribSocial' => number_format($contribSocial,2,'.',''),
            'irrfCalc' => number_format($irrfCalc,2,'.',''),
            'extraIrrfCalc' => number_format($extraIrrfCalc,2,'.',''),
            'percPis' => number_format($percPis,2,'.',''),
            'percCofins' => number_format($percCofins,2,'.',''),
            'cost' => number_format($cost,2,'.',''),
            'margemLiquida' => number_format($margemLiquida,2,'.',''),
            'receitaLiquida' => number_format($receitaLiquida,2,'.',''),
            'custoVariavel' => number_format($custoVariavel,2,'.',''),
            'lucroLiquidoValor' => number_format($lucroLiquidoValor,2,'.',''),
            'lucroLiquidoPerc' =>number_format($lucroLiquidoPerc,2,'.',''),
            // credit: credit
        ];
    }

//    public static function cal_tax_product($icms, $sell_price,
//                                           $contribSocial, $irrf, $extraIrrf,
//                                           $percPisPresumido, $percCofinsPresumido,
//                                           $mediumCost, $typeProduct, $taxCode){
//        /**
//         * Se for importado ou fora do Estado o credito é 4%
//         *1,2,5,6 tem 4 %
//         * MONOFASICO NAO TEM PIS/COFINS, TANTO EM 25% OU 17%
//         */
//        $percPisPresumidoCalc = $percCofinsLucroPresumido = 0;
//        $icmsValue = number_format($sell_price * $icms / 100, 2);
//        $irrfCalc = number_format($sell_price * $irrf / 100, 2);
//        $extraIrrfCalc = number_format($sell_price * $extraIrrf / 100, 2);
//        if (STR::contains($taxCode, [0, 3])) {
//            $percCofinsLucroPresumido = number_format($sell_price * $percCofinsPresumido / 100, 2);
//            $percPisPresumidoCalc = number_format($sell_price * $percPisPresumido / 100, 2);
//        }
//        $contribSocial = number_format($sell_price * $contribSocial / 100, 2);
//        $custoVariavel = $mediumCost * 0.88;
//        $creditValue = number_format($mediumCost * 0.96, 2);
//        if (Str::contains($typeProduct, [1, 2, 5, 6])) {
//            $cost = $icmsValue + $contribSocial + $irrfCalc + $extraIrrfCalc + $percPisPresumidoCalc + $percCofinsLucroPresumido + $custoVariavel - $creditValue;
//        } else {
//            $cost = $icmsValue + $contribSocial + $irrfCalc + $extraIrrfCalc + $percPisPresumidoCalc + $percCofinsLucroPresumido + $custoVariavel;
//        }
//        $costTax = $icmsValue + $contribSocial + $irrfCalc + $extraIrrfCalc + $percPisPresumidoCalc + $percCofinsLucroPresumido;
//        $cost = number_format($cost, 2);
//        $margemLiquida = number_format($sell_price * 100 / $cost - 100, 2);
//        $receitaLiquida = $sell_price - $costTax;
//        $lucroLiquidoValor = number_format($receitaLiquida - $custoVariavel, 2);
//        $lucroLiquidoPerc = number_format($lucroLiquidoValor / $sell_price * 100, 2);
//
//        $arrayReturn = array(
//            'icms_valor' => $icmsValue,
//            'creditValue' => $creditValue,
//            'ContribSocial' => $contribSocial,
//            'irrfCalc' => $irrfCalc,
//            'extraIrrfCalc' => $extraIrrfCalc,
//            'percPisPresumidoCalc' => $percPisPresumidoCalc,
//            'percCofinsLucroPresumido' => $percCofinsLucroPresumido,
//            'custo' => $cost,
//            'margemLiquida' => $margemLiquida,
//            'receitaLiquida' => $receitaLiquida,
//            'custoVariavel' => $custoVariavel,
//            'lucroLiquidoValor' => $lucroLiquidoValor,
//            'lucroLiquidoPerc' => $lucroLiquidoPerc
//        );
//        return $arrayReturn;
//    }

    public static function get_state_name_complete($state)
    {
        $estadosBrasileiros = array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        );
        return $estadosBrasileiros[$state];
    }

    public static function zeros_on_left($number, $amountZeros)
    {
        return str_pad($number, $amountZeros, "0", STR_PAD_LEFT);
    }
    public static function remove_especial_char($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
    }

    public static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i])) $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function array_diff_assoc_recursive($array1, $array2): array
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key])) {
                    $difference[$key] = $value;
                } elseif (!is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = self::array_diff_assoc_recursive($value, $array2[$key]);
                    if ($new_diff != FALSE) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif (!isset($array2[$key]) || $array2[$key] != $value) {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? [] : $difference;
    }

    public static function calc_cost($sellValue,$profit): string{
        return number_format($sellValue / (1 + $profit / 100),2);
    }

    public static function cleanup($table_name){
        DB::statement("SET @count = 0;");
        DB::statement("UPDATE `$table_name` SET `$table_name`.`id` = @count:= @count + 1;");
        DB::statement("ALTER TABLE `$table_name` AUTO_INCREMENT = 1;");
    }

    public static function checkNotificationProduct($product): bool{
        $arrayDrugs = [0,2,3,5,6,7,17];
        if (isset($product->group_id) and in_array($product->group_id,$arrayDrugs)){
            return false;
        }
        return true;
    }

    public static function isAllDaysPromotion($promotion){
        if (STR::containsAll($promotion->day_of_week,['1','2    ','3','4','5','6','7'])){
            return true;
        }
        return false;
    }
}
