<?php


namespace App\Helpers;

use App\Models\Erp\Product;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ReportsHelper{

    public static function generate_array_price_label(array $arrayCodes): array{
        $arrayData =[];
        $products = Product::select('description','presentation','sell_value','promotion_rule')
            ->join('product_extra as pe','pe.product_code','product.code')
            ->whereIn('product.code',$arrayCodes)
            ->groupBy('product.code')
            ->get();
        foreach ($products as $key =>$value){
            $price =$price_off= $value['sell_value'];
            if (strlen($value['promotion_rule']) == 10){
                $price = $price_off = self::generate_price_off_label($value['promotion_rule'],$value['sell_value']);
            }elseif(strlen($value['promotion_rule']) > 10){
                $price = self::generate_price_off_label(substr($value['promotion_rule'],0,10),$value['sell_value']);
                $price_off = self::generate_price_off_label(substr($value['promotion_rule'],10),$value['sell_value']);
            }
            array_push($arrayData, [
                'description'=>$value['description'],
                'presentation'=>$value['presentation'],
                'price'=>str_replace('.',',',$price),
                'price_off'=>str_replace('.',',',$price_off)
            ]);
        }

        $returnData['products'] = $arrayData;
        return $returnData;
    }
    public static function generate_price_off_label($promotionRule,$sellValue): string{
        Log::error($promotionRule);
        $returnString = '';
        $data =  Helper::calc_value_off( $promotionRule, $sellValue);
        foreach($data as $key =>$value){
            if (strlen($promotionRule) == 10 and $value['amount'] ==1){
                $returnString .= "{$value['price']}";
            }else {
                $returnString .= "LV {$value['amount']}PG R$ {$value['price']}cada\n";
            }
        }
        return $returnString;
    }
}
