<?php

namespace App\Exports\GuiaDaFarmacia;
use App\Helpers\ExportHelper;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Support\Facades\Log;

class Export {
    /**
     * @var mixed
     */
    private $data;
    /**
     * @var false|resource
     */
    private $file;

    public function getDataGuiaDaFarmacia(){
        $url = "https://webservice.guiadafarmaciadigital.com.br/webservice/";
        $this->file=fopen('guiaDaFarmacia.txt','w');
        $timeNow = (new Carbon())->format('dmY');
        fwrite($this->file,"01{$timeNow}" .PHP_EOL);
        for($count =1;$count<=17; $count++) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $headers = array(
                "Content-Type: application/x-www-form-urlencoded",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $dataCurl = "cnpj_cpf=03678419000357&email=financeiro@dimeoeste.com.br&senha=226488&pagina={$count}";
            curl_setopt($curl, CURLOPT_POSTFIELDS, $dataCurl);
            $resp = curl_exec($curl);
            $this->data = json_decode($resp, true);
            curl_close($curl);
            $this->writeInFile();
        }
        fclose($this->file);

    }

    public function writeInFile(){

//        return response()->json($this->data)->send();
        foreach ($this->data['data'] as $key =>$value){
            //1 - 2 = size = 02 = 02
            fwrite($this->file,ExportHelper::writeNumber(2,2));
            //3 - 8 =size 6 = ID_PRODUTO
            fwrite($this->file,ExportHelper::writeNumber($value['ID_PRODUTO'],6));
            //9 - 58 = size 50 = NOME+APRESENTACAO
            fwrite($this->file,ExportHelper::writeString("{$value['NOME']} {$value['APRESENTACAO']}",50));
            // 59 - 73 = size 14 = EAN
            fwrite($this->file,ExportHelper::writeNumber($value['EAN'],14));
            // 74- 105 =size 31 = 0
            fwrite($this->file,ExportHelper::writeNumber(0,32));
            // 106 - 145 = size 40 = XXX
            fwrite($this->file,ExportHelper::writeString('X',40,'X'));
            // 146 - 160 = size 15 = REGISTRO_MS
            fwrite($this->file,ExportHelper::writeNumber($value['REGISTRO_MS'],15));
            // 161 - 180 = size 20 = XXXXX
            fwrite($this->file,ExportHelper::writeString("X",20,"X"));
            // 181 - 190 = size 9 = 0000
            fwrite($this->file,ExportHelper::writeNumber(0,10));
            // 191 - 200 = size 10 = PRECO_FABRICA_17
            fwrite($this->file,ExportHelper::writeNumber(ExportHelper::clear($value['PRECO_FABRICA_17']),10));
            // 201 - 240 = size 40= 0000
            fwrite($this->file,ExportHelper::writeNumber(0,40));
            // 241 - 250 =size 10= PRECO_MAXIMO_17
            fwrite($this->file,ExportHelper::writeNumber(ExportHelper::clear($value['PRECO_MAXIMO_17']) ,10));
            // 250 - 281 = size 31 = 0000
            fwrite($this->file,ExportHelper::writeNumber(0,31));
            // 282 - 282 = size 1 = 1
            fwrite($this->file,ExportHelper::writeNumber(1,1));
            // 283-283 = size 1 = ID_TIPO_PRECO
            fwrite($this->file,ExportHelper::writeString($value['ID_TIPO_PRECO'],1));
            // end of line
            fwrite($this->file,PHP_EOL);
        }
    }
}
