<style>
    .no-break {
        page-break-inside: avoid;
    }
    /* force h3 title to display on new page */
    h3 {
        page-break-before: always;
    }
</style>
@foreach($products as $key =>$value)
    <!DOCTYPE html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial scale=1.0"/>
        <link href="https://fonts.googleapis.com/css?family=Quicksand:700&display=swap" rel="stylesheet">
    </head>
    <html lang="pt">
    <body style="margin:0">
        <div class="no-break">
            <div style="padding-left: 50px;border-left: 1px solid;margin-left: 6%;margin-bottom: 0px;margin-top:0px">
                <p style="font-size: 22px;line-height: 22px;text-align: center;margin:0">{{$value['description']}}</p>
                <p style="font-size: 13px;line-height: 13px;text-align: center;margin:0">{{$value['presentation']}}</p>
                <div style="height: 2px;background-color: #222;margin: 5px 0 0;"></div>
                <div style="display:flex;align-items:flex-start;justify-content: space-around;padding: 15px 0 0;margin: 0">
                    @if($value['price'] == $value['price_off'])
                        <div style="text-align: center;padding: 0; margin: 0">
                            <p style="font-size: 32px;line-height: 32px;text-align: center;padding: 0; margin: 0 0 2px">R$ {{$value['price']}}</p>
                        </div>
                    @else()
                        <div style="text-align: center;padding: 0; margin: 0">
                            <p style="font-size: 30px;line-height: 30px;text-align: center;padding: 0; margin: 0 0 5px">R$ {{$value['price']}}</p>
                        </div>
                        <div style="text-align: center;padding: 0; margin: 0">
                            <p style="font-size: 16px;line-height: 16px;text-align: center;padding: 0; margin: 5px 0 0"> {!! nl2br(e($value['price_off']))!!}</p>
                        </div>
                    @endif()
                </div>
            </div>
        </div>
        <h3></h3>
    </body>
    </html>
@endforeach
