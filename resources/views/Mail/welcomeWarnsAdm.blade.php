<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:700&display=swap" rel="stylesheet">
    <title>Novo CLiente</title>
</head>
<body style="margin:0">
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff">
    <tbody>
    <tr>
        <td width="100%">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="content-wrap">
                <tbody>
                <tr>
                    <td height="40px" bgcolor="#f5f5f5"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" bgcolor="#f5f5f5">
                            <tbody>
                            <tr>
                                <td width="30"></td>
                                <td width="180">
                                    <img style="display: block; border: 0;" src="https://iopoint.com.br/images/logo.png"  width="180" alt=""/>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="40px" bgcolor="#f5f5f5"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="left" bgcolor="#f5f5f5">
                            <tbody>
                            <tr>
                                <td width="30"></td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" align="cente">
                                        <tbody>
                                        <tr>
                                            <td style="font-family: \'Quicksand\', sans-serif;font-size: 13px;text-align: left;margin: 0;color:#1f1f1f;line-height: 16px;font-weight: 700">
                                                <font face="\'Quicksand\', sans-serif;" color="#1f1f1f">
                                                    <p> Um novo cliente acabou de se cadastrar no sistema</p>
                                                    <p>Razão Social: {{$data['name']}}</p>
                                                    <p>Nome Fantasia: {{$data['trading_name']}} </p>
                                                    <p>Nome do Usuario: {{$data['user_name']}}</p>
                                                    <p>CNPJ: {{$data['national_registry']}}</p>
                                                    <p>Telefone: {{$data['phone']}}</p>
                                                    <p>Email Empresa: {{$data['company_email']}}</p>
                                                    <p>Email Gestor: {{$data['user_email']}}</p>
                                                    <p>Código Vendedor: {{$data['seller_code']}}</p>
                                                    <p>Nome Vendedor: {{$data['seller_name']}}</p>
                                                    <p>Plano: {{$data['plan']}}</p>
                                                    <p>Preço: {{$data['price']}}</p>
                                                </font>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="30"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="30px" bgcolor="#f5f5f5"></td>
                </tr>

                <tr>
                    <td height="100" bgcolor="f5f5f5"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
