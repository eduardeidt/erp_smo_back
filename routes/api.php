<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\GenericController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductBatchController;
use App\Http\Controllers\ProductPromotionController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SubstanceController;
use App\Http\Controllers\Sync\SyncController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Authentication is not necessary
 */
Route::post('login', [AuthController::class, 'login']);

//Route::group(['prefix' => 'parameters', 'middleware' => 'jwt.auth'], function () {
Route::group(['prefix' => 'parameters'], function () {
    Route::get('', [ConfigurationController::class, 'indexParameters']);
    Route::post('', [ConfigurationController::class, 'store']);
    Route::get('guiaDaFarmacia', [ConfigurationController::class, 'guiaDaFarmacia']);
    Route::get('storeGuiaDaFarmacia', [ProductController::class, 'storeGuiaDaFarmacia']);
});

Route::group(['prefix' => 'sync', 'middleware' => 'jwt.auth'], function () {
    Route::get('', [SyncController::class, 'sync']);
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
});

/**
 * Generic
 */
Route::group(['prefix' => 'generic', 'middleware' => 'jwt.auth'], function () {
    Route::post('sendEmail', [GenericController::class, 'sendGenericEmail']);
    Route::get('downloadFile', [GenericController::class, 'downloadFiles']);
});

/*
 * Branch
 */
Route::group(['prefix' => 'branch', 'middleware' => 'jwt.auth'], function () {
    Route::get('', [BranchController::class, 'index']);
//    Route::get('{id}', [UserController::class, 'show']);
//    Route::post('', [UserController::class, 'store']);
//    Route::put('{id}', [UserController::class, 'update']);
//    Route::delete('{id}', [UserController::class, 'delete']);
});

/*
 * Home
 */
Route::group(['prefix' => 'home', 'middleware' => 'jwt.auth'], function () {
    Route::get('getHomeData', [HomeController::class, 'getHomeData']);
    Route::get('getMonthData', [HomeController::class, 'getMonthData']);
    Route::get('getYearData', [HomeController::class, 'getYearData']);
});



/**
 * Notification
 */
Route::group(['prefix' => 'notification', 'middleware' => 'jwt.auth'], function () {
    Route::get('index', [NotificationController::class, 'index']);
    Route::put('', [NotificationController::class, 'updateList']);
    Route::put('{id}', [NotificationController::class, 'update']);
    Route::delete('', [NotificationController::class, 'deleteList']);
    Route::delete('{id}', [NotificationController::class, 'delete']);
});

/**
 * Product
 */
Route::group(['prefix' => 'product'], function () {
//Route::group(['prefix' => 'product', 'middleware' => 'jwt.auth'], function () {
    Route::post('index', [ProductController::class, 'index']);
    Route::post('simple', [ProductController::class, 'getSimple']);
    Route::get('getDataForFilterProduct', [ProductController::class, 'getDataForFilterProduct']);
    Route::get('getByAutoComplete', [ProductController::class, 'getByAutoComplete']);
    Route::get('productsExpirationDate', [ProductController::class, 'getExpirationData']);
    Route::get('calcTax/{product_code}', [ProductController::class, 'calcTax']);
    Route::get('indexGuiaDaFarmacia', [ProductController::class, 'indexGuiaDaFarmacia']);
    Route::put('updateGuiaDaFarmacia', [ProductController::class, 'updateGuiaDaFarmacia']);
    Route::put('setFractionGuiaDaFarmacia', [ProductController::class, 'setFractionGuiaDaFarmacia']);
    Route::get('{id}', [ProductController::class, 'show']);
    Route::put('update', [ProductController::class, 'update']);
    Route::delete('deleteGuiaDaFarmacia', [ProductController::class, 'deleteGuiaDaFarmacia']);
    Route::delete('{id}', [ProductController::class, 'inactive']);
});



/**
 * Product Batch
 */

//Route::group(['prefix' => 'product'], function () {
Route::group(['prefix' => 'productBatch', 'middleware' => 'jwt.auth'], function () {
    Route::get('', [ProductBatchController::class, 'index']);
    Route::post('', [ProductBatchController::class, 'store']);
    Route::put('inactiveProductBatch/{id}', [ProductBatchController::class, 'inactiveProductBatch']);
    Route::put('updateExpirationDate', [ProductBatchController::class, 'updateExpirationDate']);
});


/**
 * Product - Promotion
 */
Route::group(['prefix' => 'product','middleware' => 'jwt.auth'], function () {
    Route::post('promotion', [ProductPromotionController::class, 'promotion']);
});

Route::group(['prefix' => 'productPromotion', 'middleware' => 'jwt.auth'], function () {
//Route::group(['prefix' => 'productPromotion'], function () {
    Route::get('getListToAnalyze/{id}', [ProductPromotionController::class, 'getListToAnalyze']);
    Route::get('getProducts/{id}', [ProductPromotionController::class, 'getListToAnalyze']);
    Route::get('showMissingProducts', [ProductPromotionController::class, 'showMissingProducts']);
    Route::get('importMissingProducts', [ProductPromotionController::class, 'importMissingProducts']);
    Route::get('getProductPromotionHistory', [ProductPromotionController::class, 'getProductPromotionHistory']);
    Route::post('confirmProductOnPromotion', [ProductPromotionController::class, 'confirmProductOnPromotion']);
    Route::post('insertAnalyzeItem', [ProductPromotionController::class, 'insertAnalyzeItem']);
    Route::post('effectPromotion', [ProductPromotionController::class, 'effectPromotion']);
    Route::post('importCsvToAnalyze', [ProductPromotionController::class, 'importCsvToAnalyze']);
    Route::put('update', [ProductPromotionController::class, 'update']);
    Route::delete('', [ProductPromotionController::class, 'destroy']);
    Route::delete('removeFromActivePromotion', [ProductPromotionController::class, 'removeFromActivePromotion']);
    Route::delete('nextPromotion/{id}', [ProductPromotionController::class, 'deleteNextPromotion']);
});


/*
 * Promotion
 */
//Route::group(['prefix' => 'promotion',], function () {
Route::group(['prefix' => 'promotion', 'middleware' => 'jwt.auth'], function () {
    Route::get('', [PromotionController::class, 'index']);
    Route::get('getNotInactive', [PromotionController::class, 'getNotInactive']);
    Route::get('getProductList/{id}', [PromotionController::class, 'getProductList']);
    Route::get('{id}', [PromotionController::class, 'show']);
    Route::post('', [PromotionController::class, 'store']);
    Route::put('{id}', [PromotionController::class, 'update']);
    Route::delete('{id}', [PromotionController::class, 'destroy']);
});


/*
 * User
 */
Route::group(['prefix' => 'user', 'middleware' => 'jwt.auth'], function () {
    Route::get('', [UserController::class, 'index']);
    Route::get('{id}', [UserController::class, 'show']);
    Route::post('', [UserController::class, 'store']);
    Route::put('{id}', [UserController::class, 'update']);
    Route::delete('{id}', [UserController::class, 'delete']);
});
/**
 * Reports
 *
 */
Route::group(['prefix' => 'reports', 'middleware' => 'jwt.auth'], function () {
    Route::get('productPriceLabel', [ReportController::class, 'productPriceLabel']);
});
Route::group(['prefix' => 'substance', 'middleware' => 'jwt.auth'], function () {
    Route::get('getSubstance', [SubstanceController::class, 'getSubstance']);
    Route::get('getPharmacological', [SubstanceController::class, 'getPharmacological']);
    Route::get('getTherapeutic', [SubstanceController::class, 'getTherapeutic']);
});

